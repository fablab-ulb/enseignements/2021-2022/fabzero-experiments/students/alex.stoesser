# Projet Final

*This project is placed under CC BY-NC 4.0 license, more information [here](https://creativecommons.org/licenses/by-nc/4.0/).*

Après avoir suivi les différents modules, il est maintenant temps de se lancer dans un projet concret. Pour cela, il est d'abord nécessaire de trouver une idée intéressante. Celle-ci sera ensuite réfléchie et maturée afin de pouvoir finalement réfléchir à une solution viable qui sera dévelopée. C'est le principe de la méthode Challenge-based learning qui se décompose en 3 phases "Engage", "Investigate" et "Act". Ces 3 axes forment ainsi la structure de cette documentation.


## 1.Engage
Cette première section relate la recherche d'un projet qui m'intéresse et qui aurait du sens. Il est nécessaire que ce projet me motive afin d'être impliqué dans celui-ci et de tout donner pour le réaliser dans le temps imparti qui est relativement court. Il est également important que ce projet corresponde à l'un des 17 "Sustainable development goal" (SDG), en français : les 17 objectifs de développement durable. Ces différents objectifs pour sauver le monde mis en évidence par les Nations Unies répondent aux différents défis mondiaux liés aux inégalités (de richesse, de genre, énergétiques, ...). Le but étant d'avoir atteint les 17 objectifs d'ici à 2030 afin de ne laisser personne de côté. Les 17 objectifs sont détaillés sur le [site des Nations Unies](https://sdgs.un.org/)[^1].

[^1]: https://sdgs.un.org/, visité le 10/06/22

### 1.1. Idées de problématique
J'ai donc tout d'abord réfléchi à un thème qui m'intéresserait en particulier. En parcourant les 17 SDG, un thème a attiré mon attention : "Affordable and clean energy"[^1]. Je suis en effet interessé par les énergies renouvelables notamment l'éolien et le solaire. J'ai d'ailleurs déjà eu l'occasion de travailler sur l'implémentation de panneaux solaires sur les ailes d'un drone lors de mon projet MA1.

J'ai alors réfléchi à différents projets qui pourraient m'intéresser, parmi ceux-ci les deux ayant retenus mon attention sont les suivants :

- Un système de chauffage d'eau utilisant l'énergie solaire afin de prendre une douche.

- Un support pour panneau solaire permettant une orientation optimale tout au long de la journée.

### 1.2. Choix de problématique et questions essentielles

Ensuite le moment est venu de réfléchir à quelle idée m'intéressait le plus et correspondait le mieux aux besoins de la communauté. Etant chef scout depuis 6 ans et scout depuis mon plus jeune âge, la problématique d'avoir de l'eau chaude même au milieu de nulle part m'a plu. De plus, cette problématique correspond à un deuxième SDG : "Good health and well-being"[^1]. Le projet était donc choisi : "Comment chauffer de l'eau avec l'énergie solaire ?".

### 1.3. Objectifs
Ce projet reste néanmoins assez vague et nécessitait quelques précisions.

J'aimerais en effet que ce chauffe-eau solaire soit utilisable lors de mes camps scouts mais aussi lorsque je pars en camping par exemple, c'est pourquoi le dispositif nécessite d'être transportable. La transportabilité est cependant relative aux besoins, certains partent en camping et vivent au même endroit plusieurs semaines tout en étant motorisé entre les différents campings. De la même manière, un camp scout se déroule en général à un endroit fixe durant une quinzaine de jour. Au contraire, certains voyagent et se déplacent tous les jours et ce à pied. Les critères de transportabilité nécessaires à ces 2 cas de figure sont évidemment bien différents. Dans le cadre de ce projet, on se contentera de créer un système léger et relativement petit.

Deuxièmement, comme le système doit pouvoir être utilisé n'importe où, il doit pouvoir fonctionner sans être connecté au réseau. De plus, s'il utilise de l'électricité, sa consommation doit être très faible.

Finalement, un objectif que j'aimerais me fixer est que n'importe qui ayant accès à un FabLab ou de manière générale à des outils de fabrication numérique puisse recréer le dispositif facilement et en utilisant le plus possible de matériaux récupérés.


## 2. Investigate

Cette deuxième section a pour but de se poser toutes les questions permettant de développer un dispositif répondant aux objectifs fixés à la section précédente. En effet, chercher des informations dans ce qui a déjà été fait est important pour gagner du temps. Ceci est bien résumé par la phrase "step on the shoulders of the giants not on their toes"[^2], en français "marche sur les épaules des géants et pas sur leurs orteils".

[^2]: https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/class-website/fabzero-modules/project-kickstarter/, visité le 10/06/22


### 2.1. Systèmes existants (état de l'art)
Parmi les questions à se poser lorsque l'on commence un projet, celle qui vient toujours en tête en premier est celle de l'existence ou non d'un dispositif accomplissant l'objectif visé.

Il existe en effet différents types de dispositif permettant de chauffer de l'eau grâce à l'énergie solaire. Certains sont vendus dans le commerce dans le but de chauffer des piscines ou pour le camping.
Il existe aussi différents dispositifs ayant été créés artisanalement, principalement pour chauffer des piscines.

#### 2.1.1. Douche solaire de campings

Les douches solaires de camping qui existent sur le marché sont en général des "sacs" pouvant contenir 10 à 20 litres d'eau. Ceux-ci sont fait de PVC et possèdent une face noire permettant de capturer la chaleur du soleil lorsque le sac est posé au sol. Pour se doucher, le sac est ensuite suspendu. Ce système est en général vendu aux alentours de 20 euros. J'ai déjà eu l'occasion de tester une telle douche mais malheureusement étant donné la grande quantité d'eau et la "relativement faible" surface de contact, l'eau est rarement suffisamment chaude.

![](https://fr.vidaxl.be/dw/image/v2/BFNS_PRD/on/demandware.static/-/Sites-vidaxl-catalog-master-sku/default/dw62e5d31b/hi-res/988/1011/1013/502993/502994/141122/image_1_141122.jpg?sw=400)[^3]

[^3]:https://fr.vidaxl.be/e/vidaxl-douche-solaire-dexterieur-de-camping-20-l-2-pcs/8718475876540.html?gclid=CjwKCAjw14uVBhBEEiwAaufYxzpm_Ju0QXVy0AUB-2AF9Sb6xWZCwGf55TvoY7fL9I5mZ6qeP9_xfxoC38gQAvD_BwE, visité le 10/06/22


Décathlon a créé un concept similaire mais qui a la particularité de permettre la mise sous pression de l'eau grâce à une pompe manuelle. Ce qui offre donc une meilleure pression mais également la possibilité de prendre une douche même si aucun point en hauteur n'est disponible. Il évite également de devoir porter le sac pour le mettre en hauteur car un sac de 20 kilos n'est pas soulevable par tout le monde. Ce dispositif ne peut toutefois contenir que 10 litres d'eau et est vendu 50 euros.
![](https://contents.mediadecathlon.com/p1976155/k$231723ac4258058bbf6074318513ad01/sq/douche-pression-solaire-pour-le-camping-10-litres.jpg?format=auto&f=969x969)[^4]

[^4]:https://www.decathlon.be/fr/p/douche-pression-solaire-pour-le-camping-10-litres/_/R-p-334665?mc=8650729&gclid=CjwKCAjw14uVBhBEEiwAaufYx5f6ETXNiG188J3POTV7ybHgsGwCmRezm4xar5yqrdTCq-5T6ul3_hoCtGcQAvD_BwE&gclsrc=aw.ds, visité le 10/06/22

#### 2.1.2. Chauffage de piscine

Les chauffages solaires pour piscine existent sous différentes formes et évidemment dans différentes gammes de prix. Cependant l'on peut remarquer que la méthode utilisée est similaire pour les 3 cas ci-dessous : ils utilisent un tuyau noir dans lequel l'eau circule et qui a pour but de capturer la chaleur du soleil. Les panneaux solaires ont l'avantage de pouvoir être inclinés afin de capter le plus de chaleur possible. Les panneaux solaires ont un prix très variable en fonction de leur qualité allant de 200 à 600€ minimum par mètre carré (pose non comprise). Le dome qui se trouve sur la 3ème photo coûte lui entre 100 et 150 euros. Il a l'avantage d'être plus compact mais n'est bien sûr pas adapté à tous types de piscine. La taille du dispositif solaire nécessaire pour chauffer suffisament rapidement une piscine dépend directement de son volume d'eau. Il est important de remarquer que les différents dispositifs montrés ici nécessitent tous l'emploi d'une pompe, ce qui n'est pas problématique car une piscine est en général équipée d'une pompe.

![](https://piscineinfoservice.com/wp-content/uploads/2014/08/capteur-solaire-piscine.jpg)[^5]

[^5]:https://piscineinfoservice.com/wp-content/uploads/2014/08/capteur-solaire-piscine.jpg, visité le 10/06/22

![](https://www.idees-piscine.com/wp-content/uploads/2019/05/idees-piscine-chauffage-solaire-piscine.jpg)[^6]

[^6]: https://www.idees-piscine.com/wp-content/uploads/2019/05/idees-piscine-chauffage-solaire-piscine.jpg, visité le 10/06/22

![](https://fr.vidaxl.be/dw/image/v2/BFNS_PRD/on/demandware.static/-/Sites-vidaxl-catalog-master-sku/default/dw5abf3360/hi-res/536/729/2832/5654/424865/image_1_424865.jpg?sw=400)[^7]

[^7]:https://fr.vidaxl.be/dw/image/v2/BFNS_PRD/on/demandware.static/-/Sites-vidaxl-catalog-master-sku/default/dw5abf3360/hi-res/536/729/2832/5654/424865/image_1_424865.jpg?sw=400, visité le 10/06/22



#### 2.1.3. Miroir

Des systèmes complexes utilisant des miroirs pour concentrer les rayons du soleil sur un tuyau contenant de l'eau existent également. Ceux-ci sont néanmoins assez peu utilisés actuellement. Ces systèmes sont principalement utilisés dans des centrales électriques.

![](https://www.revolution-energetique.com/wp-content/uploads/2019/06/centrale_solaire_concentration-1024x714.jpg)[^8]

[^8]:https://www.revolution-energetique.com/wp-content/uploads/2019/06/centrale_solaire_concentration-1024x714.jpg, visité le 10/06/22


Les miroirs utilisés peuvent être courbes mais aussi planes. Les miroirs planes sont alors montés sur une structure amovible permettant de modifier leur inclinaison au long de la journée pour garder une réflexion des rayons solaires vers le tube à chauffer. Utiliser des miroirs planes permet de réduire les coûts liés à la création des miroirs, celle des miroirs paraboliques étant particulièrement couteuse. Les miroirs planes ont cependant une efficacité légèrement plus faible.[^9]

[^9]: https://www.revolution-energetique.com/avenir-radieux-pour-le-solaire-a-concentration/, visité le 10/06/22



#### 2.1.4. Systèmes artisanaux non commercialisés

Certains systèmes ont bien sûr déjà été réalisés par différentes personnes et peuvent être trouvés sur internet. Ceux-ci se basent sur les différentes technologies mentionnées précédemment.

Premièrement, un exemple utilisant simplement un "récipient noir" est présenté dans cette vidéo des Marioles Trotters. Ce couple voyage depuis maintenant quelques années en van à travers l'Afrique. Afin de se doucher, ils ont accroché un tuyau noir sur leur galerie de toit permettant le stockage de plusieurs dizaines de litres d'eau qui sont chauffés grâce à l'énergie solaire. Pour avoir de la pression pour se doucher, ils n'utilisent pas une pompe mais un compresseur qu'ils avaient déjà à bord permettant de regonfler les pneus de leur véhicule ayant été préalablement dégonflés pour traverser une dune ou rouler sur une plage par exemple. Ils mentionnent dans la vidéo que l'eau après une journée au soleil est "bouillante".[^10]

<iframe width="560" height="315"
src="https://www.youtube.com/watch?v=9Lran0bNW7I" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen>
</iframe>
[^10]





[^10]: https://www.youtube.com/watch?v=9Lran0bNW7I, visité le 10/06/22


Deuxièmement, des systèmes de panneaux solaires sont développés comme dans la vidéo ci-dessous. Cette vidéo a été choisie car elle comporte différents points interpellants et qui semblent contraires à un bon fonctionnement du dispositif. Cela sera discuté plus tard sur cette page. Malgré tout, ce dispositif est très proche de celui ayant été construit dans le cadre de ce projet comme vous pourrez l'observer plus loin.

<iframe width="560" height="315"
src="https://www.youtube.com/watch?v=fGPWChUel3k&t=518s" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen>
</iframe> [^11]


[^11]:https://www.youtube.com/watch?v=fGPWChUel3k&t=518s , visité le 10/06/22

Troisièmement, dans cette vidéo, on peut observer un dispositif utilisant des miroirs afin de concentrer les rayons solaires sur un tuyau contenant de l'eau. Bien que dans cet exemple, le volume d'eau est très limité, un système de circulation d'eau pourrait être ajouté pour chauffer un plus grand volume. L'eau atteint avec ce dispositif son point d'ébullition. Attention toutefois, cette température se trouvant aux alentours des 100°c, cela rend l'utilisation du dispositif dangereuse.

<iframe width="560" height="315"
src="https://www.youtube.com/watch?v=gvaObFjOuWQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen>
</iframe> [^12]


[^12]: https://www.youtube.com/watch?v=gvaObFjOuWQ, visité le 10/06/22


### 2.2. Questions additionelles
Après avoir observé tout ce qui se fait déjà, viennent les questions liées à la réalisation de mon projet. Voici les différentes questions que je me suis posées et les réponses qui ont été apportées.

- Combien de litres d'eau pour une douche ?

Bien qu'une douche à la maison consomme en général plusieurs dizaines voir centaines de litres d'eau, un pommeau classique ayant un débit de 20 à 30 litres par minute [^13], une douche peut se limiter à beaucoup moins d'eau que cela, c'est pourquoi les systèmes de douches solaires abordés à la section 2.1.1. ont en général une capacité de 10 à 20 litres [^3] [^4]. Notre système devrait donc idéalement avoir une capacité du même ordre de grandeur : 10 litres pour une douche limitée à une seule personne, parce qu'un réservoir de 20 litres peut être assez compliqué à transporter. L'idéal serait un système adaptable à différents réservoirs.

[^13]:https://www.engie.be/fr/blog/conseils-energie/le-vrai-et-le-faux-sur-notre-consommation-deau/, visité le 10/06/22

- Quelle est la température idéale d'une douche ?

La température recommandée d'une douche est comprise entre 36 et 38°C, température idéale pour nettoyer la peau sans l'irriter. Pour les peaux plus sensibles une température légèrement inférieure est recommandée, environ 33 à 35°C. [^19] La température d'une douche chaude "agréable" est cependant située légèrement au-dessus de la température du corps pour la plupart des gens soit entre 37 et 40 degrés.[^20]

[^19]: https://fenetresurvie.com/quelle-temperature-pour-une-douche/, visité le 10/06/22
[^20]: https://steemit.com/fr/@lefactuoscope/fr-pourquoi-vous-devriez-prendre-des-douches-froides, visité le 10/06/22


- Quel système utiliser pour chauffer l'eau ?

Voici donc la question la plus importante : quelle méthode utiliser ? Ayant pu expérimenter une douche solaire de type "sac", j'ai toujours eu un avis mitigé sur celle-ci car elle chauffe en effet l'eau mais très lentement et ce principalement à cause du manque de surface d'échange de chaleur. En effet, un échange de chaleur entre 2 milieux est directement lié à leur surface de contact. Le flux de chaleur a en effet pour unité des Watts par mètre carré (W/m²). Plus la surface entre les 2 milieux sera grande, plus la chaleur sera transmise d'un milieu à l'autre. Pour augmenter l'échange de chaleur, il faut donc la plus grande surface possible par volume d'eau contenu. Des fins tuyaux sont donc optimum. Ce qui me ménera vers la création d'un dispositif de type panneau solaire. [^14]

[^14]: http://www.fast.u-psud.fr/~mergui/2A101/cours_thermique_L2.pdf, visité le 10/06/22


- Comment faire circuler l'eau ?

Afin de faire circuler l'eau à travers les tuyaux, mon but initial était d'utiliser la gravité. De cette manière, aucune électricité n'est nécessaire au fonctionnement du dispositif.

Une autre méthode consiste évidemment à utiliser une pompe.

- Quelle taille est acceptable pour considérer le sytème transportable ?

Comme mentionné précédemment, la notion de transportabilité est très variable en fonction des conditions et des besoins de déplacement. J'ai donc décider de fixer la taille du panneau à celle d'une malette. Il apparait qu'une malette fait en général environ 45x35cm. La taille du panneau devrait donc être dans les alentours de 40x40cm.

- Comment capter un maximum de chaleur ?

Il y a plusieurs points clés pour capter un maximum de chaleur. Premièrement, comme cela a pu être observé dans presque tous les dispositifs présentés ci-dessus, la couleur du matériau joue beaucoup sur la captation de chaleur. En effet, un matériau foncé aura tendance à capter la chaleur du soleil en stockant les rayons infrarouges. Il aura aussi tendance à plus facilement transmettre sa chaleur, ce qui est intéressant pour chauffer del'eau. [^15] Un noir mat aura plus tendance à capter la chaleur qu'un noir brillant car ce dernier réfléchit une partie des rayons du soleil l'atteignant. [^10]
Un deuxième point, lié à l'absorption de chaleur est le matériau utilisé. En effet certains matériaux captent ou conservent (pierre, brique) mieux la chaleur que d'autres. [^16]

[^15]:https://www.pourlascience.fr/sr/idees-physique/s-habiller-en-noir-pour-se-proteger-du-soleil-3402.php, visité le 10/06/22

[^16]:https://www.futura-sciences.com/maison/dossiers/batiment-inertie-thermique-cle-maison-870/page/3/#:~:text=Des%20mat%C3%A9riaux%20qui%20stockent%20la,b%C3%A9ton%20conviennent%20pour%20cet%20usage., visité le 10/06/22

Un troisième moyen d'emmagasiner de la chaleur est d'utiliser l'effet de serre, bien connu, pour son rôle dans le réchauffement climatique. En effet l'effet de serre emprisonne les rayons du soleil entre la surface de la terre et les différentes couches de l'atmosphère, réchauffant celle-ci. Cet effet tire bien évidemment son nom des serres agricoles utilisant le même procédé pour créer un environnement plus chaud que l'extérieur.

![](https://www.wikidebrouillard.org/images/thumb/9/96/Effet_de_serre_effet_de_serre_oieau.png/ia-a1ca167b69b406db4f0f5388925cbabf-1000px-Effet_de_serre_effet_de_serre_oieau.png.png)[^17]

[^17]:https://www.wikidebrouillard.org/images/thumb/9/96/Effet_de_serre_effet_de_serre_oieau.png/ia-a1ca167b69b406db4f0f5388925cbabf-1000px-Effet_de_serre_effet_de_serre_oieau.png.png, visité le 10/06/22

Finalement, la dernière manière d'optimiser la captation de chaleur est d'avoir un angle optimal entre les rayons du soleil et la surface à chauffer mais aussi une bonne orientation. De la même manière qu'un panneau photovoltaïque sera plus efficace s'il est bien exposé, notre panneau solaire devra être placé idéalement.

L'exposition optimale d'un panneau solaire est lorsque celui-ci est perpendiculaire à la direction des rayons solaires. Etant donné que notre panneau est amovible, celui-ci pourrait-être déplacé au long de la journée.
Cependant le soleil se levant à l'est et se couchant à l'ouest en passant par le sud, la meilleur orientation d'un panneau sera de l'orienter côté sud si l'on souhaite ne pas le déplacer. De la même manière pour espérer avoir la meilleure orientation, un panneau solaire doit (en Belgique !) être incliné de 30 à 35 degrés. [^18]
![](https://www.ecohabitatbelge.be/wp-content/uploads/2021/04/design-orientation-panneau-solaire.png) [^18]

[^18]: https://www.ecohabitatbelge.be/inclinaison-et-orientation-des-panneaux-solaires-resultats-reussis-et-parfaits/#:~:text=Dans%20la%20norme%2C%20la%20meilleure,produire%20le%20maximum%20d'%C3%A9lectricit%C3%A9., visité le 10/06/22


Un résumé de l'efficacité d'un panneau photovoltaïque en fonction de son orientation et de son inclinaison est disponible ci-dessous :
![](https://www.ecohabitatbelge.be/wp-content/uploads/2021/04/Tableau-orientation-et-inclinaison-1.png)[^18]

## 3. Act


Cette troisième section parle du développement du dispositif. Celui-ci s'est passé en plusieurs étapes qui vont être expliquées ci-dessous.

### 3.1. Première idée
La première idée consistait en l'utilisation d'un tuyau noir sur une planche de bois noire également. Ce tuyau mènerait ainsi l'eau d'un réservoir d'entrée à un réservoir de sortie en passant par un panneau solaire permettant de la chauffer. Le réservoir d'entrée étant placé plus haut que celui de sortie, cela permet de faire circuler l'eau uniquement par la gravité. Pour que l'eau ait le temps de chauffer suffisamment, un robinet serait placé afin de réguler le flux d'eau et donc sa vitesse pour passer d'un réservoir à l'autre.

#### 3.1.1. Premier prototype
Lors de la réalisation d'un premier prototype, plusieurs problèmes ont été rencontrés. C'est en effet à ça que sert le prototypage rapide : faire des essais afin de se rendre compte des limitations qui vont être rencontrées à la réalisation.

Pour permettre la circulation de l'eau, des serpentins de tuyaux devaient être effectués horizontalement. Seulement, pour effectuer ces serpentins, le tuyau doit effectuer une boucle de 180°. Faire cette boucle sans plier le tuyau prend bcp de place et ne convient pas à un dispositif relativement petit comme on désire le réaliser. Pour rendre le système plus compact, des coudes de 180° ont été conçus sur OpenSCAD.

![](imageprojetfinal/coude180.jpg)

Mais cette idée a finalement été rejetée car elle demanderait l'utilisation de beaucoup de coudes et l'impression en 3D de ceux-ci ne serait pas frugale et prendrait beaucoup de temps.


### 3.1.2. Deuxième prototype

Une manière d'éviter l'utilisation de coudes est de ne plus utiliser des serpentins mais bien une spirale. Un test a été fait avec un tuyau placé en spirale, pour cela une plaque a été découpée dans du carton (pour le premier test), celle-ci comporte des trous à des espacements réguliers permettant d'accrocher le tuyau en spirale.


![](imageprojetfinal/tuyauspirale.jpg)


Cependant si le tuyau est placé en spirale, l'eau ne descend plus tout au long de son parcours mais doit également être capable de remonter. Un test a été effectué et l'eau est capable de circuler d'un réservoir à l'autre. Cependant, cela nécessite que le réservoir de départ soit placé assez haut par rapport au reste du système afin de profiter de plus d'énergie potentielle. Cette hauteur dépend également de l'inclinaison du panneau : si le panneau est horizontal, l'eau ne doit pas remonter, si au contraire il est placé verticalement, l'eau s'écoule beaucoup plus difficilement.

J'ai alors pensé à faire une sorte de malette avec 2 faces de panneau solaires qui se replieraient l'une sur l'autre. Les côtés permettraient un positionement du réservoir de départ en hauteur et une pente assez faible des 2 panneaux permettant la circulation de l'eau. De cette manière, le réservoir n'aurait pas besoin d'être accroché en hauteur, ce qui n'est pas spécialement possible partout.

![](imageprojetfinal/idée1.jpg)

Cette structure est assez compliquée à mettre en place car le réservoir ne pourra pas être placé très haut et la structure de la malette doit donc être capable de soutenir un poids de 10 à 20kg sur une de ses extrémités. De plus, l'angle d'inclinaison des panneaux serait difficilement optimale. Doubler la surface des panneaux permet de doubler l'absorption de chaleur par l'eau mais double également le prix du dispositif.

Après quelques tests, l'eau ne semble pas avoir le temps de chauffer après un passage dans le panneau. Une autre idée devait donc être envisagée.



### 3.2. Deuxième idée

L'essentiel étant l'efficacité du dispositif, le but premier étant que ça chauffe. Un système isolant le tuyau de l'air a alors été pensé de manière à créer un effet de serre. De plus le système se doit d'utiliser une pompe. Celle-ci permet de faire recirculer l'eau, en effet un seul réservoir est utilisé. L'eau est donc prélevée de celui-ci, passe dans le panneau pour être réchauffée et est ensuite reversée dans ce même réservoir. Ce qui permet d'éviter le fait que l'eau s'écoule trop vite sans avoir le temps de réellement chauffer d'un réservoir à l'autre. Cela permet aussi de réduire le nombre de réservoirs nécessaires, réduisant le cout, le volume et le poids du système. En faisant recirculer l'eau, un seul panneau est suffisant, même si l'eau chaufferait plus vite en en utilisant 2. Réduire le nombre de panneaux réduit également le prix car moins de longueur de tuyau est utilisée.

Comme mentionné ci-dessus, le design a donc pour but de créer un effet de serre. Dans cette "serre" un tuyau noir est placé en spirale, lui permettant de capter la chaleur des rayons du soleil. Ce design est donc assez proche de celui présenté dans la section 2.1.4 mais à quelques différences près. Comme mentionné avant, le design observé présente des problèmes. Premièrement, il utilise un fond réfléchissant. Ce fond réfléchissant va dès lors réfléchir les rayons du soleil et la chaleur sera moins absorbée. Même si le but recherché est un effet de serre, il est important que la chaleur soit absorbée par la surface afin de la rendre à l'intérieur de la serre. Si la surface réfléchit les rayons lumineux, une majorité de rayons seront réfléchis et ressortiront de la serre. Le deuxième problème est l'absence de pompe alors qu'il n'y a qu'un seul réservoir. La gravité ne va donc pas faire circuler l'eau, même si une arrivée d'eau est plus basse que l'autre.

![](https://i0.wp.com/newphysicist.com/wp-content/uploads/2021/01/scrnli_1_28_2021_11-18-09-AM.png?w=829&ssl=1)[^21]

[^21]: https://i0.wp.com/newphysicist.com/wp-content/uploads/2021/01/scrnli_1_28_2021_11-18-09-AM.png?w=829&ssl=1, visité le 10/06/22

Le défi est maintenant de créer une boite fermée et isolée permettant un phénomène d'effet de serre.
- Pour créer un effet de serre, une couche transparente comme du verre ou du plexiglass est nécessaire sur le dessus de la boite. En dessous de cette couche transparente, se situe la "serre" dans laquelle sera situé le tuyau. Pour absorber un maximum de chaleur, le fond de la serre ainsi que le tuyau seront peints avec de la peinture noire mat.

- Le deuxième défi consiste en garder un maximum de chaleur à l'intérieur de la serre et pour cela, ses parois seront isolées. La face transparente ne peut pas être isolée car l'isolation de celle-ci empêcherait les rayons solaires d'entrer. La paroi du fond par contre est celle sur laquelle le plus de chaleur est concentrée et nécessite donc d'être isolée le mieux possible. Pour cela, sous la partie "serre" du panneau, une couche d'isolant sera placée. Les parois verticales étant beaucoup plus petites que le fond, elles seront donc moins sujettes à une perte de chaleur. Cependant les isoler est évidemment l'idéal.

- Finalement, il faut trouver un moyen de créer une boite fermée de manière assez simple mais qui soit robuste et légère. Un plus est l'utilisation de matériaux de récupération. Pour créer cette boite, la solution la plus évidente était alors de découper des pièces à la découpeuse laser dans des plaques de bois (MDF). Pour assembler les différentes pièces, la technique "T-nut and bolt" décrite [ici](https://www.bigbluesaw.com/articles-list/big-blue-saw-designing-for-waterjet/construction-techniques-tab-and-slot-with-t-nut-construction.html)[^21] est utilisée.

[^21]:https://www.bigbluesaw.com/articles-list/big-blue-saw-designing-for-waterjet/construction-techniques-tab-and-slot-with-t-nut-construction.html, visité le 10/06/22

#### 3.2.1. Mise en place du prototype
La première chose à faire est de choisir un tuyau. Comme mentionné précédemment, plus celui-ci est fin plus la surface d'échange de chaleur est grande. Il faudrait donc le tuyau le plus fin possible. Cependant, un tuyau trop fin créera plus de frottement avec l'eau et une plus grande force (Hmax pour une pompe) sera nécessaire pour faire circuler l'eau. Pour choisir le tuyau optimal, 2 tuyaux d'un mètre ont été achetés chez Brico. L'un faisant 6mm de diamètre intérieur et 10 mm de diamètre extérieur et le second de 8 et 12 mm respectivement. Il a été observé que l'eau circulait mieux dans le second. Le tuyau de 8mm de diamètre intérieur et 12 mm de diamètre extérieur a donc été choisi. Le tuyau est souple et fait en PVC ce qui le rend assez résistant aux UV. Cette étape avait été réalisée lors la toute première étape de prototypage de la première idée. Le tuyau étant déjà acheté, il a été gardé pour la suite.

Ensuite ayant chez moi une plaque d'isolant (en polystirène) de 2cm d'épaisseur, la construction du panneau a été basée sur les dimensions de cette plaque et des tuyaux. Les dernières dimensions manquantes étant celle des matériaux de construction de la boite en elle-même. J'espèrais initialement utiliser des chutes de mdf disponibles au FabLab mais aucune chute ne permettait de faire des plaques de 40x40cm, j'ai donc acheté une planche de mdf de 3mm d'épaisseur. Pour la couche transparente, j'ai trouvé une plaque de PS de 50cm de côté qui convenait parfaitement, celle-ci fait 2,5mm d'épaisseur.
La boite en mdf formant le panneau mesurera donc 40x40cm sur 4 cm d'épaisseur (2 cm d'isolant, 1,2cm de tuyaux, 2 couches de mdf 6mm et une couche de PS 2,5mm).


Avant de découper les pièces, la technique T-bolt and nuts devait être testée. Celle-ci a été testée sur des petites pièces. Il était en effet important de trouver les bonnes dimensions de découpe correspondant au vis et aux écrous choisis, à savoir des M3. Les vis font 16mm de long. Au cours de ces tests, la dimensions des découpes a été réduite au maximum. A côté des attaches vis/écrou, des encoches sont créées afin d'emboiter les pièces. Ces encoches faisaient initialement 5mm sur les 2 pièces mais étant donné la largeur de saignée du laser, il y avait un peu de jeu. Une pièce a donc gardé des encoches de 5mm et l'autre des encoches de 4 et 6mm respectivement pour les "trous" et pour les "parties saillantes". Les 2 pièces tiennent alors fermement l'une dans l'autre. Lors de la création de la boite, certaines pièces avaient des bords légèrement obliques ne permettant pas l'emboitement des deux pièces, ceux-ci ont alors été limés.

![](imageprojetfinal/Tnut.jpg)

Les différentes pièces ont été dessinées sur inkscape. Sur l'image ci-dessous, l'on peut observer en haut à gauche, la plaque du fond de la boite se plaçant sous l'isolant. En haut à droite se trouvent les 4 côtés verticaux. L'un d'eux possède 2 trous additionels permettant de passer le tuyau, ces trous font exactement 12mm de diamètre afin que le tuyau y passe mais qu'il n'y ait pas de jeu laissant sortir de l'air chaud de la serre. Au milieux des côtés l'on remarque une découpe rectangulaire qui permet d'y caler la plaque centrale soutenant le tuyau. Les trous permettant d'accrocher le tuyau avec des colsons ont été réduits en taille et rapprochés les uns des autres, permettant de mieux les accrocher mais aussi de réduire les pertes de chaleur par ces trous. Cette plaque est celle visible en bas à gauche. Finalement, en bas à droite se trouve la découpe à faire dans la plaque de PS. Les différents fichiers sont disponibles dans le dossier [Files](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/alex.stoesser/-/tree/main/docs/Files) de ma page Gitlab.

![](imageprojetfinal/decoupe.png)

Lors de la création des pièces, quelques erreurs de dimensions ont été effectuées, les pièces s'emboitaient alors plutôt mal. Quelques photos sont montrées ci-dessous:

Au départ, la longueur des côtés a été mal réfléchie et ils ne s'emboitaient pas bien verticalement :

![](imageprojetfinal/malemboit.jpg)

En essayant de régler ce problème, j'ai créé un décallage dans l'une des deux attaches horizontales :

![](imageprojetfinal/decale.jpg)

Finalement, les pièces s'emboitent correctement :
![](imageprojetfinal/bienemboit.jpg)

Pour éviter les problèmes de dimensions, des pièces en carton ont ensuite été effectuées pour faire des tests et ne pas perdre de mdf ni de PS. Un de ces tests, pour la plaque de PV est observé ci-dessous :
![](imageprojetfinal/testPS.jpg)
Comme celui-ci était satisfaisant, la plaque en PS a ensuite été découpée. Pour connaitre les paramètres de découpe optimaux, des tests ont été faits en découpant des petits carrés dans la plaque :
![](imageprojetfinal/testPS2.jpg)
Les réglages optimum sur la Lasersaur étaient une vitesse de 600mm/min et une puissance de 100W pour la plaque de PS 2,5mm et respectivement de 800 mm/min et 100W pour la plaque de MDF 3mm.

Une fois toutes les découpes effectuées, l'ensemble a pu être assemblé. En prenant soin de placer la plaque d'isolant à l'intérieur, d'accrocher le tuyau à la plaque peinte en noir mat puis de repeindre avec le tuyau accroché.  Deux derniers problèmes ont finalement été rencontrés:

- Premièrement, je comptais faire passer le morceau de tuyau arrivant au centre de la spirale au dessus de cette dernière. Cependant, ayant omis ce détail lors du dimensionnement de la boite, ce n'était pas possible. Je l'ai alors fait passer sous la spirale, dans la couche d'isolant. Pour cela, 2 trous ont été forés dans la plaque noire. Idéalement, ces trous auraient dû être découpés à la découpeuse laser.

- Deuxièmement, vu le système d'attache des différentes plaques, les côtés verticaux de la serre présentent des trous permettant de grosses pertes de chaleur. Ces côtés ont dès lors été isolés grâce une fine couche de liège trouvée dans les chutes du FabLab.

Le résulat final est alors le suivant :


![](imageprojetfinal/panneau.jpg)

### 3.2.2. Pompe et réservoir

Comme mentionné précédemment, un seul réservoir va être utilisé et une pompe permettra la circulation de l'eau de celui-ci à travers le panneau.

Nul besoin d'une grosse pompe ici, cependant étant donné que celle-ci doit certainement être utilisée sans réseau, il faut qu'elle consomme peu et soit branchable sur une batterie portable. La pompe choisie est dès lors une pompe pour aquarium. Celle-ci se branche en USB, ce qui permet de l'utiliser avec une simple batterie de type powerbank. Elle se branche en courant continu et prend 3.5 à 8V en entrée, une batterie offrant en général 3,7 ou 5V. De plus, cette pompe ne consomme que 1 à 3W. Elle a un Hmax de 0,4 à 1,5m correspondant à la "hauteur relative" à laquelle elle peut élever de l'eau. On parle ici de hauteur relative car dans cette valeur sont aussi inclus les frottements entre l'eau et le tuyau. Cette valeur est tout à fait suffisante. Le débit maximum (Qmax) de cette pompe est de 200L/H max. Qmax peut être varié grâce à un cache venant se mettre devant l'entrée d'eau de la pompe. C'est assez pratique car pour pouvoir chauffer suffisamment l'eau, il faut que celle-ci passe lentement dans le panneau solaire. Au contraire, pour se doucher, augmenter le débit peut être agréable. Tout en restant assez faible pour ne pas utiliser l'eau trop rapidement.

![](https://m.media-amazon.com/images/I/71dIRr9o-kL._AC_SX679_.jpg)[^22]

[^22]:https://m.media-amazon.com/images/I/71dIRr9o-kL._AC_SX679_.jpg, visité le 10/06/22

Du point de vue du réservoir, n'importe quel réservoir peut être utilisé, de n'importe quel volume tant que celui-ci permet d'y introduire la pompe. Les réservoirs de 10 ou 20 litres ont en général des ouvertures assez grandes, qui permettent d'y introduire la pompe de 4,7(avec la tige)x4x2,5cm. J'ai personnelement opté pour un seau avec couvercle et j'ai troué le couvercle pour laisser passer les tuyaux et le cable de la pompe.

![](imageprojetfinal/réservoir.jpg)


### 3.2.2. Matériel utilisé

| Quantité |  Description    |  Prix  |          Où le trouver ?         | Notes  |
|-----|-----------------|---------|--------------------------|--------|
| 10m | Tuyau 8-12mm PVC  |  2,49€/m = 24,9€ | Brico   |     |
| 1   | Plaque de MDF 3mm  (122x61cm)  |  6,49€ | Brico   |        |
| 1   | Plaque de PS 2,5mm (50x50cm) |  9,29€ | Brico   |  Récupéré     |
| 1   | Plaque de polystirène 2cm (40x40cm) | 2,5€/m² | Brico  |  Récupéré      |
| 1   | Pompe pour aquarium  |  9,99€|    amazon.com [^23]    |   |
| 1   | Colsons |  2,95€ | Brico  |   Prix pour 200 colsons mais seuls 31 sont  nécessaires    |
| 1   | Bombe de peinture noir mat | 10,68€ | bol.com   |  Récupéré   |
| 1   | Vis et écrou M3 (sachet de 30) | 3,89€ | Brico | Seuls une vingtaine sont nécessaires       |
| 1   | Liège| 10€/m² | Brico   |   Récupéré (besoin de très peu)   |
| 1   | Seau avec couvercle | 3€ (seau 12L)|  BricoDepot |    Récupéré    |
| 1   | Batterie portable |  |    |    Déjà en ma possesion   |

[^23]:https://www.amazon.fr/flintronic%C2%AE-Submersible-Aquarium-immerg%C3%A9e-Ultra-Silencieux/dp/B07TW39QXP/ref=asc_df_B07TW39QXP/?tag=&linkCode=df0&hvadid=353838670300&hvpos=&hvnetw=g&hvrand=13514151234510927301&hvpone=&hvptwo=&hvqmt=&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=9040071&hvtargid=pla-829849445924&ref=&adgrpid=71550315975&th=1

Le montage entier m'a donc couté moins de 50 euros, ce qui correspond au prix de la douche de camping de chez Décathlon. Bien sûr l'achat en gros de certains matériaux aurait fait descendre le prix du dispositif.

### 3.3. Résultats
Le dispositif a été testé le samedi 11 juin 2022, sous une température extérieure d'environ 20-23 degrés. Le ciel était voilé avec des éclaircies. Le panneau a été orienté vers le Sud et incliné à environ 32-33°.
8 Litres d'eau ont été placés dans le dispositif à 13h10. La température de l'eau était alors de 20°c.
A 15 heures, les 8L d'eau avaient atteint une température de 33,6°c et l'eau sortant du panneau était à 36,2°c.
A 17 heures, l'eau dans le seau atteignait les 38,9°c et l'eau sortant du panneau était à 39,9°c.
Finalement, à 18heures, l'eau avait atteint 41°c dans le réservoir et 42,8°c en sortie de tuyau. Au terme de ces 5heures de test, la batterie n'avait pas encore perdu un quart de sa capacité (soit 2500mAh). Le dispositif étant branché en 3,7V à une puissance variant normalement entre 1 et 3 W, il tire à priori entre 0,27 et 0,81A. Soit 1350 à 4050mAh en 5heures.

On voit donc que la température augmente de moins en moins rapidement. Ce qui est certainement lié au conditions climatiques de la journée et l'alternance de nuages et d'éclaircies mais aussi lié au dispositif en lui-même et à son isolation qui n'est pas parfaite. D'un autre côté, plus le temps avance dans la journée moins la puissance fournie par le soleil est grande.
Cette température est toutefois totalement suffisante pour se doucher, il serait même préférable de rajouter de l'eau froide avant de se doucher.

Le fait que l'eau ait atteint 39°c en 4 heures alors que le soleil était très intermittent est très rassurant et confirme le bon fonctionnement du dispositif. Cependant, avec 20 litres d'eau, le temps nécessaire pour atteindre cette température aurait bien sûr été plus long.
![](imageprojetfinal/final.jpg)
![](imageprojetfinal/Final2.jpg)
![](imageprojetfinal/thermomètre.jpg)

### 3.4. Améliorations possibles

Pour avoir de meilleures performances quelques améliorations sont possibles:

- La plaque sur laquelle le tuyau est fixé est actuellement en bois, on pourrait la remplacer par une plaque en matériau qui absorbe et surtout qui rend mieux la chaleur par exemple en métal.

- De la même manière, le tuyau actuellement en PVC pourrait être en métal.

- Le tuyau pourrait être plus long car il n'occupe pas actuellement le centre du panneau. Ou alors le panneau pourrait être plus petit pour être plus facilement transportable.

- L'isolation peut aussi être améliorée. En effet, la plaque supérieure et inférieure laissent passer un peu d'air par endroit. Les attaches en "T-nut and bolt" créant des trous dans les parois ne sont également pas idéales pour l'isolation.

- La plaque de PS transparente réfléchit beaucoup la lumière, une plaque réflechissant moins permettrait une plus grosse absorption de chaleur dans la serre.

- Pour améliorer la transportabilité, il serait intéressant d'ajouter une poignée ou un système pour porter le panneau sur son dos.

- Finalement une dernière amélioration qui pourrait être faite n'est pas directement liée à la structure en elle-même. Etant donné que le système est adaptable à différent réservoirs, il devrait également être facilement adaptable en superficie. Pour cela, il serait intéressant de créer un outil qui fournit les fichiers SVG adaptés à une surface demandée. De la même manière que l'outil de Nicolas De Coster permet de créer des filets kirigamis (voir [ici](https://fabacademy.org/2018/labs/fablabulb/students/nicolas-decoster/?page=assignment&assignment=03#FlexWoodLaser)[^24]).

[^24]:https://fabacademy.org/2018/labs/fablabulb/students/nicolas-decoster/?page=assignment&assignment=03#FlexWoodLaser, visité le 10/06/22
## 4. Conclusion

L'objectif de ce projet était de créer un système capable de chauffer de l'eau grâce à l'énergie solaire pour permettre à une
personne de se doucher. Le système se devait d'être transportable et ne pas nécessiter d'être relié au réseau (faible consommation électrique). Toutes ces conditions ont été remplies avec succès, le système est capable de chauffer suffisamment d'eau pour qu'une personne puisse se doucher à une température de 40°c et ce même si le temps n'est pas parfaitement dégagé. Le panneau fait la taille d'une malette, soit 40x40x4cm et ne pèse que 2kg (vide). Le système est par ailleurs adaptable à différentes tailles de réservoir. Finalement, le système consomme de l'électricité mais peut être alimenté grâce à une batterie portable et consomme très peu. De plus, le système est assez facile à reconstruire avec des outils de fabrication numérique et peut être majoritairement construit avec des matériaux de récupération.



<!--- This is a markdown comment which this extension removes.

## Gallery

![](imageprojetfinal/cubecentré.jpg)

## Video

### From Vimeo

<iframe src="https://player.vimeo.com/video/10048961" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/10048961">Sound Waves</a> from <a href="https://vimeo.com/radarboy">George Gally (Radarboy)</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### From Youtube

<iframe width="560" height="315" src="https://www.youtube.com/embed/jjNgJFemlC4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## 3D Models

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Dita&#39;s Gown</a>
    by <a href="https://sketchfab.com/francisbitontistudio?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Francis Bitonti Studio</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div>
-->
## 5. Bibliographie :
