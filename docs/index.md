Bonjour,
Je m'appelle Alexandre Stoesser et vous êtes sur ma page du cours "How to Make (almost) Any Experiment Using Digital Fabrication". Sur celle-ci vous pourrez retrouver ma documentation des différents modules du cours ainsi que l'avancement de mon projet.
## A propos de moi
Comme dit précédemment, je m'appelle Alexandre Stoesser. Je suis né à Bruxelles en janvier 2000 et vit chaque instant comme si c'était le dernier depuis lors.
J'étudie à l'Ecole Polytechnique de Bruxelles et suis en dernière année de master en électromécanique, option aéronautique.
En dehors des études, je joue au hockey et je suis chef scout depuis 5 ans.
![](https://scontent.fbru5-1.fna.fbcdn.net/v/t1.6435-9/52985903_2094105820680457_6635469291133075456_n.jpg?_nc_cat=103&ccb=1-5&_nc_sid=09cbfe&_nc_ohc=hDZDJXnEejYAX-_sDJT&_nc_ht=scontent.fbru5-1.fna&oh=00_AT9w7ABHrmpamEssU5bevaTB6_sGJHpovSJ1hDoObeRhyQ&oe=623D80A0)
## Mon mémoire
Je fais actuellement mon mémoire au Frugal Lab. Celui-ci porte sur la recherche ainsi que le développement à grande échelle d'un filet à nuage afin de l'envoyer à une ONG travaillant au Maroc.
![Filet à nuage Cloud Fisher installé sur le Mont Boutmezguida au Maroc](https://pyxis.nymag.com/v1/imgs/5d9/5dc/75c5d281c5c98bb2535132304e88f188e4-25-fog-net.rsquare.w700.jpg)

