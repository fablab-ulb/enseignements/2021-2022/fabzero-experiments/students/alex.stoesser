$fn=500;

epaisseur=3;
longueur_de_la_tige=30;
distance_entre_les_trous = 8;
longueur_du_bloc=20;
largeur_du_bloc=7;
largeurtrou=5;


largeurprincipale=largeur_du_bloc;
longueurprincipale=longueur_du_bloc;



width=1; depth = longueur_de_la_tige; height= epaisseur;


x0=0; y0=depth/2+longueurprincipale/2-0.1; z0=0; 

x1=0; y1=y0+distance_entre_les_trous/2; z1=0; 

x2=0; y2=y0-distance_entre_les_trous/2; z2=0; 

union(){
cube([width,depth,height], center= true);
bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, longueurprincipale, largeurprincipale, epaisseur, largeurtrou) ; 

mirror([0,1,0]) bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, longueurprincipale, largeurprincipale, epaisseur, largeurtrou);
    
     
}


module bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, longueurprincipale, largeurprincipale, epaisseur, largeurtrou){
    
 

    difference(){
    rectanglearrondi(x0,y0,z0,longueurprincipale, largeurprincipale, epaisseur);

    translate([x1,y1,z1]) cylinder(epaisseur+0.1, d= largeurtrou, center=true);
    
    translate([x2,y2,z2]) cylinder(epaisseur+0.1, d= largeurtrou, center=true);
    }
}

module rectanglearrondi(x0,y0,z0,longueurprincipale, largeurprincipale, epaisseur){
    
    union(){
    translate([x0,y0,z0]) cube([largeurprincipale,longueurprincipale-largeurprincipale, epaisseur], center=true);
    
    translate([x0,y0-(longueurprincipale-largeurprincipale)/2,z0])cylinder(epaisseur, d=largeurprincipale, center=true);
    
    translate([x0,y0+(longueurprincipale-largeurprincipale)/2,z0])cylinder(epaisseur, d=largeurprincipale, center=true);
    }
}