$fn=500;
epaisseur=5;

largeurprincipale=5;
longueurprincipale=10;

largeurtrou=2.5;

width=1; depth = 40; height= epaisseur;

x1=0; y1=2; z1=0; 
x2=0; y2=-2; z2=0; 
x3=-width/2; y3=longueurprincipale/2-0.1; z3=-height/2; 
x4=0; y4=longueurprincipale+depth-0.2; z4=0;
x5=0; y5=longueurprincipale+depth+1.8; z5=0;
x6=0; y6=longueurprincipale+depth-2.2; z6=0;

union(){
difference(){
    scale([1.0,longueurprincipale/largeurprincipale,1]) cylinder(epaisseur, d = largeurprincipale, center=true);

    translate([x1,y1,z1]) cylinder(epaisseur+0.1, d= largeurtrou, center=true);
    
    translate([x2,y2,z2]) cylinder(epaisseur+0.1, d= largeurtrou, center=true);
}


translate([x3,y3,z3]) cube([width,depth,height]) ; 

difference(){
    translate([x4,y4,z4]) scale([1.0,longueurprincipale/largeurprincipale,1]) cylinder(epaisseur, d = largeurprincipale, center=true);

    translate([x5,y5,z5]) cylinder(epaisseur+0.1, d= largeurtrou, center=true);
    
    translate([x6,y6,z6]) cylinder(epaisseur+0.1, d= largeurtrou, center=true);
}
}