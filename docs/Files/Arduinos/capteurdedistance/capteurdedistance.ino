/* This example shows how to use continuous mode to take
range measurements with the VL53L0X. It is based on
vl53l0x_ContinuousRanging_Example.c from the VL53L0X API.

The range readings are in units of mm. */

#include <Wire.h>
#include <VL53L0X.h>

VL53L0X sensor;
int led = 2;
int mm;
void setup()
{
  Serial.begin(9600);
  Wire.begin();

  sensor.init();
  sensor.setTimeout(500);

  // Start continuous back-to-back mode (take readings as
  // fast as possible).  To use continuous timed mode
  // instead, provide a desired inter-measurement period in
  // ms (e.g. sensor.startContinuous(100)).
  sensor.startContinuous();
  pinMode(led, OUTPUT);
  }

void loop()
{
  mm=sensor.readRangeContinuousMillimeters();
  Serial.print(mm);
  if (sensor.timeoutOccurred()) { Serial.print(" TIMEOUT"); }

  Serial.println();
  
    if (mm < 100){
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  }
        
  else {
    digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
    }  
}
