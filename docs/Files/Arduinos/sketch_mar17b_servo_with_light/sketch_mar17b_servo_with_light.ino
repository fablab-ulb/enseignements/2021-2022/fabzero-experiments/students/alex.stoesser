#include <Servo.h>

Servo myservo;  // create servo object to control a servo

int sensor= A1;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
myservo.attach(A0);  // attaches the servo on pin 9 to the servo object
pinMode(sensor, INPUT); 
}

void loop() {
  // put your main code here, to run repeatedly:
  // read the input pin:
  int buttonState = analogRead(sensor);
  // print out the state of the button:
  Serial.println(buttonState);
  delay(1);        // delay in between reads for stability



  int Off =250;
  int On = 10;
  if (buttonState > 900){
  myservo.write(Off);                  
  }
  else {
  myservo.write(On);                   
  }                      
}
