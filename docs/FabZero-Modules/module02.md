# 2. Conception Assistée par Ordinateur

Ce deuxième module est dédié d'abord aux flexions et aux mécanismes conformes et ensuite à la conception assistée par ordinateur notamment sur les logiciels OpenSCAD et FreeCAD.


## 2.1. Flexions et mécanismes conformes

Un mécanisme conforme (compliant mechanism) est un mécanisme flexible qui permet de transformer une force mécanique d'entrée en une force mécanique de sortie. L'un des plus anciens mécanisme conforme est l'arc à flèche mais de nombreux autres exemples existent dans notre vie de tous les jours comme par exemple le coupe-ongle.

![](https://www.togi-sante.com/media/produit/img/pince-coupe-ongle-incarne--32478--comed-1_700x700.jpg)

Grâce à ces mécanismes flexibles, il est par exemple possible de créer des rotations ou des déplacements parallèles entre différentes pièces assez aisément. Il est aussi possible de créer des mécanismes bistables qui comme leur nom l'indique ont deux positions rendant le mécanisme stable.
De tels mécanismes peuvent être observés [ici](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/class/-/raw/main/vade-mecum/videos/bistable-4-bars.mp4) et [ici](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/class/-/raw/main/vade-mecum/videos/switch.mp4).


## 2.2. Les logiciels de conception assistée par Ordinateur

Ces logiciels permettent de designer des pièces qui pourront par la suite être imprimée à l'aide d'imprimantes 3D. Il existe énormément de tels logiciels comme par exemple :

- Solidworks
- Fusion360
- OpenSCAD
- FreeCAD

Les 2 premiers étant des logiciels privés et les 2 suivants étant opensources. Ces logiciels permettent aussi très souvent de faire des simulations que ce soit du point de vue résistance des matériaux et de la structure mais permettent aussi parfois de faire de la CFD (Computational Fluid Dynamics) computation.

Dans le cadre de ce cours, les logiciels seront utilisés pour l'impression 3D et les logiciels opensource présentés précédemment correspondent parfaitement à cela. C'est pourquoi OpenSCAD et FreeCAD seront utilisés ci-dessous.

### 2.2.1. OpenSCAD

OpenSCAD est un logiciel très facile à prendre en main car les objets 3D sont entièrement créés grâce à du code. Une fois l'objet défini (codé), le code peut être compilé afin d'obtenir une préview de l'objet ainsi créé.

Sur la photo ci-dessous on observe à gauche le code permettant de créer un cube de 10 de côté et son illustration à droite après avoir cliqué sur le bouton entouré en rouge.

![](../images/CompileOpenSCAD.png)

Aucune unité n'est prise en compte dans le logiciel. Ce n'est que lors de l'utilisation du fichier que des unités prennnent sens, par exemple lors de l'impression 3D. Il est dès lors interessant de travailler dans une unité du système international. Le plus simple étant de travailler en millimètre.

Ce qui rend OpenSCAD particulièrement facile à prendre en main c'est la simplicité de son langage de programmation. La majorité des fonctions disponibles peuvent être trouvées [dans cette cheatsheet](https://openscad.org/cheatsheet/).

#### 2.2.1.1. Créer des objets
Les objets tridimensionnels principaux peuvent ainsi être créé de la manière suivante.

##### Cube et parallélipipède rectangle

Cube :
```
cube(taille_des_côtés);
```

Parallélipipède rectangle :
```
cube([largeur, profondeur, hauteur]);
```

##### Sphère
```
sphere(rayon);
ou
sphere(d= diamètre);
```

##### Cylindre
Pour un cylindre droit :
```
cylinder(hauteur, rayon);
ou
cylinder(hauteur, d= diamètre);
```
Pour un "cylindre" avec 2 bases de rayons différents :
```
cylinder(hauteur, rayon1, rayon2);
ou
cylinder(hauteur, d1= diamètre1, d2= diamètre2);
```

#### 2.2.1.2. Positioner ses objets

Il est tout d'abord important de savoir qu'un objet est de base placé avec l'un de ses coins en (x=0, y=0, z=0), mais il est également possible de le centrer sur ce point en rajoutant un paramètre à l'objet créé comme ceci :
```
cube(taille_des_côtés, center =true);
```
Cette action est bien sûr possible avec n'importe quel objet créé (à savoir qu'une sphère est créée de base avec son centre en l'origine). En comparaison avec le cube montré au point 2.2.1., cette fois le cube sera placé comme ceci :
![](../images/cubecentré.jpg)

Ensuite, afin de déplacer l'objet la fonction translate peut être utilisée en mentionnant de quelle distance l'objet doit être déplacé dans chaque direction.

```
translate([x,y,z]) cube(taille_des_côtés, center = true);
```

Par exemple ici, un cube de 10 de côté centré en [50, 0 , 0] :
![](../images/cubetranslate.jpg)

#### 2.2.1.3. Créer des objets complexes à l'aide d'objets élémentaires

Des opérateurs booléens peuvent être utilisés pour assembler plusieurs objets. Certains de ces opérateurs sont union et difference permettant respectivement d'additioner ou de soustraire certaines objets entre eux.

##### Union
L'opérateur union doit être utilisé comme ceci :
```
union(){
  cube(côté);
  sphere(rayon);
}
```


Un exemple d'union de deux objets est montré ci-dessous :
![](../images/union.jpg)

##### Différence

L'opérateur différence permet de soustraire un ou plusieurs objets au premier objet cité comme suit.
```
difference(){
  cube(côté);
  sphere(rayon);
}
```
Ici, la sphère sera donc retirée du cube. Dans les 2 exemples suivant, une sphère est d'abord retirée d'un cube puis inversément.
![](../images/diff1.jpg)
![](../images/diff2.jpg)

#### 2.2.1.4. Autres fonctions intéressantes

D'autres fonctions assez intéressantes peuvent également être utilisées comme mirror() qui permet de faire une symmétrie d'un objet ou encore module() qui permet de créer une "fonction" pouvant être appelée ensuite dans le code.

Les fonctions intersection() et hull() permettent respectivement d'assembler des objets en ne gardant que leurs intersections et de former une "coque" contenant les 2 objets.
Un exemple de hull() est observé ci-dessous, on observe aisément la différence avec l'union des 2 mêmes objets au point 2.2.1.3. .
![](../images/hull.jpg)

Une liste assez exhaustive de fonctions est disponibles [ici](https://openscad.org/cheatsheet/).

#### 2.2.1.5 Paramétrisation

Il est intéressant de paramétriser ses designs afin de pouvoir assez facilement modifier ce dernier sans devoir modifier tout son code.

Pour cela, à la place d'utiliser le code suivant :
```
cube(10);
```
On utilisera :
```
cote=10;
cube(cote);
```

De cette manière, si l'on veut ensuite créer une sphère dont le rayon vaut 2 fois, le côté de notre cube nous pourront simplement utiliser :
```
cote=10;
cube(cote);
sphere(cote*2);
```

Modifier la valeur du côté à la ligne 1 modifierait alors directement le côté du cube ainsi que le rayon de la sphère.



## 2.3. FreeCAD

FreeCAD contrairement à OpenSCAD n'utilise pas directement de code pour créer des objets mais permet la création de lignes, de formes et d'objets 3D manuellement. Chaque objet créé ayant un certain nombre de degré de liberté, il est important de contraindre chacun de ceux-ci ou du moins tous ceux que l'on veut contraindre. Il est également possible d'extruder une pièce. Une vidéo montrant comment faire ceci est disponible [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/computer-aided-design/-/blob/master/FreeCAD.md).

FreeCAD permet de faire tout ce que OpenSCAD permet de faire mais est toutefois pour certains un peu plus compliqué à prendre en main. La paramétrisation est également possible à l'aide de spreadsheets.

Etant donné que l'on ne travaille pas en code mais bien manuellement à la conception des objets, FreeCAD pourrait être sensible aux changements de version tout comme l'est Solidworks.

FreeCAD a cependant certains avantages comme par exemple pour arrondir tous les coins liés à une face. En effet, cette action peut être faite en une fraction de seconde et pour différents coins en même temps sur FreeCAD, là où sur OpenSCAD il serait nécessaire de coder cet arrondi pour chaque coin et prendrait bien plus de temps.


## 2.4. Conception d'une pièce en 3D permettant des mécanismes conformes

Comme exercice, il nous a été demandé de designer une pièce permettant des mécanismes conformes comme celle sur la photo suivante :
![](https://cdn.thingiverse.com/renders/81/30/c8/e1/b2/cab3067ee17924ac5df6a13ad1259d0b_preview_card.jpg)

Etant donné que nous ne connaissons aucune dimension de cette objet, il est plus qu'intéressant de le réaliser de manière paramétrique afin de pouvoir modifier les dimensions de l'objet une fois qu'elles auront été déterminées.

J'ai réalisé cette pièce sur OpenSCAD. Ma première réalisation est celle-ci :
```
/*
    FILE    : Flexible3D.scad

    AUTHOR  : Stoesser Alexandre <alexandre.stoesser@ulb.be>

    DATE    : 2022-03-22

    License : CC BY (https://creativecommons.org/licenses/by/4.0/)
*/


$fn=500;
epaisseur=5;

largeurprincipale=5;
longueurprincipale=10;

largeurtrou=2.5;

width=1; depth = 40; height= epaisseur;

x1=0; y1=2; z1=0;
x2=0; y2=-2; z2=0;
x3=-width/2; y3=longueurprincipale/2-0.1; z3=-height/2;
x4=0; y4=longueurprincipale+depth-0.2; z4=0;
x5=0; y5=longueurprincipale+depth+1.8; z5=0;
x6=0; y6=longueurprincipale+depth-2.2; z6=0;

union(){
difference(){
    scale([1.0,longueurprincipale/largeurprincipale,1]) cylinder(epaisseur, d = largeurprincipale, center=true);
    translate([x1,y1,z1]) cylinder(epaisseur+0.1, d= largeurtrou, center=true);
    translate([x2,y2,z2]) cylinder(epaisseur+0.1, d= largeurtrou, center=true);
}

translate([x3,y3,z3]) cube([width,depth,height]) ;

difference(){
    translate([x4,y4,z4]) scale([1.0,longueurprincipale/largeurprincipale,1]) cylinder(epaisseur, d = largeurprincipale, center=true);
    translate([x5,y5,z5]) cylinder(epaisseur+0.1, d= largeurtrou, center=true);
    translate([x6,y6,z6]) cylinder(epaisseur+0.1, d= largeurtrou, center=true);
}
}
```

Ce qui donne ceci :

![](../images/exercice1.jpg)

Cet objet bien que proche de l'objectif ne correspondait pas exactement à mes attentes notamment parce que les 2 bouts étaient sensés être plus rectangulaires. J'ai donc fait une deuxième version de cet objet, tout en profitant de cette occasion pour tester la fonction mirror() ainsi que la fonction module(). J'en ai aussi profité pour mettre en évidence les paramètres modifiant les dimensions de l'objet dans les premières lignes afin que cette manipulation soit plus aisée et explicite.

```
/*
    FILE    : Flexible3Dv2.scad

    AUTHOR  : Stoesser Alexandre <alexandre.stoesser@ulb.be>

    DATE    : 2022-03-22

    License : CC BY (https://creativecommons.org/licenses/by/4.0/)
*/

$fn=500;

epaisseur=3;
longueur_de_la_tige=30;
distance_entre_les_trous = 5;
longueur_du_bloc=10;
largeur_du_bloc=4;
largeurtrou=2.5;

largeurprincipale=largeur_du_bloc;
longueurprincipale=longueur_du_bloc;
width=1; depth = longueur_de_la_tige; height= epaisseur;
x0=0; y0=depth/2+longueurprincipale/2-0.1; z0=0;
x1=0; y1=y0+distance_entre_les_trous/2; z1=0;
x2=0; y2=y0-distance_entre_les_trous/2; z2=0;

union(){
cube([width,depth,height], center= true);
bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, longueurprincipale, largeurprincipale, epaisseur, largeurtrou) ;
mirror([0,1,0]) bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, longueurprincipale, largeurprincipale, epaisseur, largeurtrou);   
}


module bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, longueurprincipale, largeurprincipale, epaisseur, largeurtrou){

    difference(){
    rectanglearrondi(x0,y0,z0,longueurprincipale, largeurprincipale, epaisseur);
    translate([x1,y1,z1]) cylinder(epaisseur+0.1, d= largeurtrou, center=true);
    translate([x2,y2,z2]) cylinder(epaisseur+0.1, d= largeurtrou, center=true);
    }
}

module rectanglearrondi(x0,y0,z0,longueurprincipale, largeurprincipale, epaisseur){

    union(){
    translate([x0,y0,z0]) cube([largeurprincipale,longueurprincipale-largeurprincipale, epaisseur], center=true);
    translate([x0,y0-(longueurprincipale-largeurprincipale)/2,z0])cylinder(epaisseur, d=largeurprincipale, center=true);
    translate([x0,y0+(longueurprincipale-largeurprincipale)/2,z0])cylinder(epaisseur, d=largeurprincipale, center=true);
    }
}
```

Le résultat final étant alors :

![](../images/exercice2.jpg)
