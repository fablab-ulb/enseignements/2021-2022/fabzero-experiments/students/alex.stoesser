# 6. Electronique 2 - Fabrication

Après avoir travaillé sur le prototypage éléctronique la semaine passée à l'aide d'un Arduino, il est maintenant temps de fabriquer un équivalent à ce dernier. Les Arduinos sont basiques et assez chers pour les performance qu'ils peuvent fournir alors que des équivalents plus performants peuvent être créés pour un prix de l'ordre de l'euro. Il est en général utilisé principalement parce que son fonctionnement est facile à prendre en main et que de nombreux projets ont déjà été réalisés grâce à des processeurs de la marque.  

Arduino utilise un code très transparent ce qui le rend très utile. Il suffit en effet de lui dire "Je veux que cette pin soit une Output et de lui envoyer une suite de 0 et de 1" pour utiliser une LED par exemple. Cependant, ces commandes demandent beaucoup de configuration au niveau du software.

Si l'on veut utiliser l'IDE Arduino, il est donc nécessaire de réécrire une librairie pour chaque types de chip permettant de faire le lien entre son code source et celui de la librairie Arduino. En général, passer par la librairie Arduino pour faire briller une LED demande une centaine de commande là où seulement 2, 3 commandes pourraient être utilisées. Cependant utiliser le code source est bien plus compliqué.

En général, lors de l'achat d'un chip, celui-ci contient déjà un code qui permet d'y envoyer des nouveaux programmes. Etant donné qu'ici nous construisons notre Arduino depuis le tout début, il faudra y implémenter un "bootloader".

## 6.1. La conception du PCB

L'année dernière, le PCB a été réalisé au labo mais pour des raisons de temps ainsi que de précision, les PCB ont été réalisé cette année par une entreprise. Nous avons donc reçu notre PCB près à recevoir tous ses composants.

![](https://scontent.fbru4-1.fna.fbcdn.net/v/t1.15752-9/277030100_1165386820901171_6207205556886389554_n.jpg?_nc_cat=106&ccb=1-5&_nc_sid=ae9488&_nc_ohc=gT1NqvLEsUEAX9XLH_e&_nc_ht=scontent.fbru4-1.fna&oh=03_AVJiAdmgAIIskX0LgR3wredzjWM-bDjbe5nUQqTTrmplaw&oe=62694B45)

## 6.2. La soudure

La principale tache de ce module était donc de souder tous les composants nécessaire à notre PCB. Les différents composants sont :

- un microprocesseur Atmel SAMD11C
- un régulateur de tension
- 2 condensateurs
- 2 LEDS, 1 toujours allumée lorsque la board est connectée et une qui peut être utilisée grâce à un programme (LED_BUILTIN)
- 3 résistances (deux de 3,3 kOhms pour les LEDs et une de 1001 Ohms)
- 2 fois 6 pins permettant la connection d'éléments extérieurs

La première chose à faire était donc de souder le microprocesseur, pour cela, il faut souder ses 14 pattes. Il est très important de vérifier que le microprocesseur est placé dans le bon sens avant de le souder. Dans notre cas, lorsque le PCB est placé avec la prise USB vers le bas, le microprocesseur doit être placé avec le "point de référence" en haut à gauche.

![](https://scontent.fbru4-1.fna.fbcdn.net/v/t1.15752-9/276973826_770422040590971_2078064903962472712_n.jpg?_nc_cat=111&ccb=1-5&_nc_sid=ae9488&_nc_ohc=yLP3ZNuuPtIAX-Vh1G5&_nc_ht=scontent.fbru4-1.fna&oh=03_AVIPExg4FLXqLlNXZiMrlND-nj_yzBOVU4hvM_pJ3pAmGQ&oe=62678135)

Pour souder un composant, il faut faire fondre le fil d'étain grâce au fer à souder afin d'en récupérer une petite goutte que l'on viendra appliquer entre la patte du microprocesseur et l'endroit du PCB prévu à cet effet. Il est important de s'assurer qu'un contact est bien établi entre les deux.

Lors de la soudure plusieurs problèmes peuvent survenir :

- un trop gros dépot de métal
- la création d'un pont entre 2 pins adjacentes
- un mauvais contact entre la pin et le PCB ne laissant pas passer de courant

Les deux premiers peuvent être rattrapés grâce à une pompe à déssouder. Celle-ci permet d'aspirer le métal liquide (il faut donc le chauffer grâce au fer à souder), il est toutefois parfois nécessaire de rajouter de la matière afin de permettre une meilleure aspiration.

![](https://www.manutan.be/img/S/GRP/ST/AIG333185.jpg)

Un mauvais contact entre la pin et le PCB est parfois difficile à voir à l'oeil nu et même à la loupe et sera observé grâce à un multimètre qui permet de savoir si le courant passe ou non jusqu'à cette pin. Dans le cas contraire, il faut ressouder la pin.

Après le microcontrolleur, il faut de la même manière souder les 4 pins du régulateur de tension.

![](https://scontent.fbru4-1.fna.fbcdn.net/v/t1.15752-9/276979529_653522335905605_5086178656790901677_n.jpg?_nc_cat=106&ccb=1-5&_nc_sid=ae9488&_nc_ohc=VZL5tWJe5ZQAX-b9YYb&_nc_ht=scontent.fbru4-1.fna&oh=03_AVIWH5RV_2GiP0GMxT_ICiahWPzdMGjlV3asojOjmWImeQ&oe=6267F157)

Une fois cette étape faite, il suffit de souder le condesateur C2 et les pins pour pouvoir uploader le bootloader sur la carte. Cela n'a pas marché du premier coup car certaines de mes pins n'étaient pas correctement soudées mais avec un peu de persévérance et de tests grâce à un multimètre, toutes les pins ont finalement été soudées correctement et le bootloader a pu être installé.

Il ne restait alors plus que les différentes LEDS, résistances et le dernier condensateur et le microcontrolleur pouvait être utilisé.

![](https://scontent.fbru4-1.fna.fbcdn.net/v/t1.15752-9/276976323_699034307946104_1908575510048370086_n.jpg?_nc_cat=107&ccb=1-5&_nc_sid=ae9488&_nc_ohc=0HBPcjixHlIAX9SDPQO&_nc_ht=scontent.fbru4-1.fna&oh=03_AVJ_oV0sIGQrroICBXpC0GoNLOz3vjx1jebKn7pwm9zDCw&oe=6268E278)


## 6.3. Utilisation de la carte

Malheureusement, comme d'autres personnes du groupe, malgré que toutes les soudures semblaient correctes et que le bootloader ai pu être installé, la carte n'était pas reconnue par nos ordinateurs et était donc inutilisable.

De multiples vérifications ont alors été faites à l'aide d'un multimètre. Le multimètre permet ainsi de vérifier que le courant passe bien entre les différentes pins ayant été soudée. Il s'est alors avéré que la pin 4 du microcontrolleur n'était pas bien soudée, celle-ci étant liée au 3.3V donc le canal de communication de l'USB, rendait donc la détéction par USB impossible. Une fois cette pin soudée, la carte est reconnue correctement.

Afin de pouvoir l'utiliser, il est nécessaire de faire certains réglages. Il faut premièrement ajouter
```
https://www.mattairtech.com/software/arduino/package_MattairTech_index.json
```
au manager de board puis l'installer. Ensuite il faut faire quelques réglages arduino, choisir la board : Generic D11C14A, la clock : INTERNAL_USB_CALIBRATED_OSCILLATOR, l'USB-CONFIG : CDC_ONLY, le SERIAL_CONFIG : ONE_UART_ONE_WIRE_NO_SPI et finalement le BOOTLOADER : 4kB Bootloader qui est celui qui a été uploader dessus précédemment.

Des vidéos et plus amples explications des réglages sont disponibles [ici](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero/electronics/-/blob/master/Make-Your-Own-Arduino.md).

Après que tous ces réglages aient été faits, il ne reste plus qu'à uploader un code et vérifier que la carte fonctionne correctement. C'est là que j'ai remarqué deux nouveaux problèmes, en effet, deux des soudures présentent un faux contact surement du à une mauvaise adhésion de l'étain de la soudure au PCB. Ces deux soudures sont indiquées par une flèche sur la photo ci-dessous.
![](../images/Module6/flèches.jpg)

Premièrement la LED rouge, sensée être allumée tant que la carte est connectée ne s'allume pas du à une mauvaise soudure de sa résistance. Deuxièmement, la LED verte qui est une LED qui peut être utilisée grâce à un code ne fonctionne pas à cause d'une mauvaise soudure au niveau du GND. Néanmoins en appliquant une légère pression sur chacune de ses soudures, on peut observer leur fonctionnement. Afin de terminer le travail correctement, il serait bien sûr nécessaire de refaire ces 2 soudures.

La LED rouge :

![](../images/Module6/lumière_rouge.jpg)

Le clignotement irrégulier de la LED verte :


<video width="500" controls src="../../videos/lumière_verte.mp4" type="video/mp4">
Résultat
</video>


Ce clignotement correspond au code suivant :
```
/*
File : "blink.ino"

Author : Alexandre Stoesser <alexandre.stoesser@ulb.be>

License : CC BY (https://creativecommons.org/licenses/by/4.0/)

*/


int led=15;
void setup() {
  pinMode(led, OUTPUT);
}

void loop() {
  digitalWrite(led, HIGH);
  delay(2000);                     
  digitalWrite(led, LOW);   
  delay(1000);
    digitalWrite(led, HIGH);
  delay(500);                     
  digitalWrite(led, LOW);   
  delay(1000);                         
}
```
Il est interessant de remarquer que la LED verte ne peut pas être utilisée comme sur une carte Arduino grâce à LED_BUILTIN mais doit être utilisée comme une LED "externe" branchée sur la pin 15.



<!-- ATTENTION on peut bruler la carte si on connecte du 5V à une mauvaise pin (il y a qu'une seule pin en 5V et même celle là ne peut pas être lié aux autres sinon c'est foutu). PArce que la communication d'un usb est en 3.3V et le ship aussi du coup. -->
