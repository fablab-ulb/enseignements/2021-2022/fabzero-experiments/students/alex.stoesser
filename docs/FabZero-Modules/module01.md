# 1. Documentation
Le premier module de ce cours porte sur la documentation, soit exactement ce que je suis en train de faire. La documentation, c'est simple, c'est un compte rendu de ce qui a été appris durant le cours. Mais au delà de ça, c'est aussi un moyen simple mais efficace de ne pas oublier ce qui a été vu ainsi et surtout qu'un moyen de partager son savoir avec les autres. Pour cela, Gitlab est utilisé.

## 1.1. Modifier son site
Chacun a son propre site déjà créé sur Gitlab mais il est nécessaire de savoir le modifier, pour ce faire il est possible de modifier directement sur le Web IDE en cliquant ici :

![](../images/WEB_IDE.png)


Cependant, il est bien plus intéressant et rapide de travailler en local sur son ordinateur. Pour ce faire, il faut tout d'abord installer Git. Ensuite, il faut lier son compte Git avec son ordinateur à l'aide d'une clé SSH. Finalement, en clonant le dossier git sur son pc, ce dernier est cloné et peut être modifié en local. Il est dès lors possible depuis Bash d'utiliser les commandes "pull" et "push" afin de télécharger les fichiers du site et de les uploader une fois modifiés. Tout ce ceci est expliqué précisément et étapes par étapes sur [ce site](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#install-git).

Etant donné que j'utilise Atom comme éditeur de texte, il est possible de "push" directement sans passer par Bash. Pour cela, il suffit de cliquer sur "Git" en bas à droite de la fenêtre Atom (1). Cela ouvre alors un outil permettant de push comme observé sur la photo ci-dessous. Les modifications n'ayant pas encore ajoutées à Gitlab sont alors affichées dans "Unstaged changes" et peuvent être ajoutées en cliquant sur "Stage all" (2).


![](../images/Atom_push1.png)



Elles apparaissent alors dans "Staged change" et un message de commit peut être ajouté avant de cliquer sur "Commit to main" (3).

Une fois que le message de commit a été écrit et que les fichiers ont été "commit to main", le bouton "Fetch" devient "Push" (4) et permet d'ajouter les fichiers à Gitlab.

![](../images/Atom_push3.png)



## 1.2. Mise en page du texte

Etant donné  que je débute en Markddown, j'ai du apprendre comment l'utiliser et j'ai donc recensé ici les principales commandes. Bien d'autres commandes peuvent être trouvées sur [ce site](https://www.markdownguide.org/basic-syntax/).
### 1.2.1. Titres
Afin d'écrire les titres, il suffit de mettre un, deux, trois (ou plus) # en fonction du niveau d'importance du titre.

```
# Titre 1
## Titre 1.1
### Titre 1.1.1
# Titre 2
```

### 1.2.2. Gras et italique
Pour écrire en italique, il faut mettre le texte entre simple astérisque.
```
*Texte en italique*
```
Pour écrire en gras, il faut mettre le texte entre double astérisque.
```
**Texte en gras**
```


### 1.2.3. Images, vidéos et liens

Une image est placée en utilisant :
```
![alt text](image.jpg)
```

et un lien, de manière assez similaire :

```
[title](https://www.example.com)
```

Finalement, pour placer une vidéo, le plus simple est de le faire en html :
```
<video width="500" controls src="../../CheminDeLaVideo.mp4" type="video/mp4">
Commentaire
</video>
```

Attention, même si cela peut paraitre bizarre, il est nécessaire de mettre ../../ avant la vidéo. A la place de width, "height" peut également être utilisé pour régler la taille de la vidéo (les 2 peuvent également être utilisés ensembles).



## 1.3. Version control
Il est important de savoir que Git fonctionne à l'aide du version control. C'est à dire que chaque version du site est enregistrée. Ainsi à chaque fois qu'une nouvelle version du code texte est "push" vers Gitlab, celle-ci est enregistrée à tout jamais. Ce qui permet de toujours savoir récupérer une ancienne version de ses documents, même si ceux-ci ont été modifiés par après et n'apparaissent plus dans la version finale du site.



## 1.4. Réduire ses images et vidéos
Etant donné que chaque version est enregistrée, les informations stockées peuvent très vite devenir conséquentes surtout si des images ou vidéos sont utilisées. Il est donc important de savoir réduire la taille de celles-ci pour éviter un stockage de données excessifs et non-nécessaire.

Pour les images différents logiciels peuvent être utilisés comme :

- Gimp
- GraphicsMagick
- ImageMagick
- Photoshop
- ...

Ces outils permettent non seulement de changer la taille d'une image mais aussi différentes retouches. Réduire de 20% le nombre de pixel d'une image n'affecte pas ou très légèrement la qualité d'une image mais réduit grandement son poids sur les serveurs.  

De manière générale, pour les images le format .jpg sera préféré à .png car plus léger bien que de moins bonne qualité.

De la même manière différents logiciels peuvent être utilisés pour réduire la qualité d'une vidéo: kdenlive peut être utilisé pour éditer des vidéos, là où Handbrake peut être utilisé pour convertir des vidéos. Cette fois-ci le format .mp4 sera préféré.

La taille d'une vidéo est considérée comme suffisamment faible lorsqu'elle fait moins de 10Mo/min. Une manière assez simple de réduire la taille d'une vidéo est d'installer FFMPEG sur Bash et d'utiliser [ces commandes](http://academy.cba.mit.edu/classes/computer_design/video.html) pour entre autres réduire la taille d'une vidéo.

Une autre très bonne manière de faire, étant donné que la plupart des vidéos sont prises sur téléphone est de les modifier directement dessus. Sur android (en tous cas sur un téléphone Huawei), l'application "Petal Clip" permet de travailler les vidéos ainsi que de réduire leur taille directement lors de l'exportation, comme sur les photos suivantes.

Il suffit d'appuyer sur "Retoucher" pour que l'application s'ouvre toute seule :

![](../images/Module1/modifier.jpg)

Une fois sur l'application, il est possible de modifier la vidéo à notre guise : la raccourcir, couper le son, rajouter de la musique, des animations, faire un montage de plusieurs vidéos, ...

![](../images/Module1/exportation.jpg)

Ensuite, en cliquant sur le bouton "EXPORTER" comme sur l'image ci-dessus, on peut choisir de réduire ou non la taille de la vidéo en réduisant la résolution mais aussi la fréquence d'image. Une estimation de la taille est directement fournie.

![](../images/Module1/réduire.jpg)


Finalement, un bon logiciel pour travailler sur des fichiers audio est Audacity mais je n'ai pas eu besoin de l'utiliser pendant les 6 séances.
