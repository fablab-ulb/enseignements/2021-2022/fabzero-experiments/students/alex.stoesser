# 4. Découpe assistée par ordinateur

La découpe assistée par ordinateur est généralement effectuée grâce à des découpeuses LASER. Celles-ci permettent une découpe très précise d'une large gamme de matériaux depuis un fichier 2D.
## 4.1. Règles de précautions
Il est tout d'abord important de prendre connaissance des différentes règles de précautions indispensables au bon fonctionnement de la machine.

### 4.1.1. Afin d'éviter tout risque d'incendie
Le risque d'incendie est sans doute le risque le plus important lors de l'utilisation d'une découpeuse LASER. Pour éviter ce dernier, il est tout d'abord impératif de prendre connaissance du **matériau** qui s'apprête à être découpé. Ensuite, il faut activer l'**air comprimé** qui permet d'éteindre les éventuelles flammes qui pourraient apparaitre. Il est également nécessaire d'allumer l'**extracteur de fumée** ainsi que le **refroidisseur à eau** sur les machines qui en sont équipées.

Finalement de manière préventive, il faut prendre connaissance de la position d'une **couverture anti-feu** ainsi que d'un **extincteur** à proximité. Ces deux éléments sont disponibles près de la porte de la salle destinée aux découpeuse LASER du fablab. Il faut également savoir où se trouve le **bouton d'arrêt d'urgence** de la machine. Finalement lors de l'utilisation d'une découpeuse LASER, l'utilisateur doit toujours **rester proche** de la machine jusqu'à la fin de la découpe afin de prévenir tout incendie.

### 4.1.2. Afin d'éviter tout risque pour la santé

D'un autre côté, certains risques pour la santé sont à prévenir. Premièrement, il ne faut **pas regarder le LASER** d'une machine ni même regarder fixement l'impact du faisceau avec le matériau découpé. D'un autre côté, pour éviter toute inhalation, il est nécessaire d'**attendre que toute la fumée ait été évacuée** avant d'ouvrir la machine.

## 4.2. Les différents matériaux que l'on peut découper

Il y a une gamme assez large de matériaux qui peuvent être découpés grâce à une découpeuse LASER, toutefois n'importe quel matériau ne peut pas être utilisé.
Que ce soit du à un risque d'incendie (carton trop épais) ou parce que le matériau réfléchirait le LASER (métaux par exemple). Toutes les infos reprises ci-dessous peuvent être retrouvées [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md).


### 4.2.1. Matériaux découpables
Les différents matériaux qui sont recommandés pour la découpe sont :

- Le papier et le carton
- Les textiles
- L'acrylique (PMMA/Plexiglass)
- Le bois contreplaqué (plywood/multiplex)


### 4.2.2. Matériaux non-découpables
Outre les matériaux découpables repris ci-dessus, certains matériaux sont soit non-recommandés soit totalement interdits.

Les matériaux non-recommandés :

- MDF : fumée épaisse et très nocive à long terme
- ABS, PS : fond facilement, fumée nocive
- PE, PET, PP : fond facilement
- Composites à base de fibres : poussières très nocives
- Métaux : impossible à découper

Finalement, les matériaux interdits :

- PVC : fumée acide et très nocive
- Cuivre : réfléchit totalement le LASER
- Téflon (PTFE) : fumée acide et très nocive
- Résine phénolique, époxy : fumée très nocive
- Vinyl, simili-cuir : peut contenir de la chlorine
- Cuir animal : juste à cause de l'odeur

## 4.3. Les différentes machines disponibles au Fablab et leur fonctionnement en bref
Trois découpeuses sont mises à disposition au Fablab. Chacune de celles-ci présente des spécifications différentes tout en ayant leurs points positifs et négatifs.

### 4.3.1. Epilog Fusion Pro 32

- Surface de découpe : 81x50 cm
- Hauteur max : 31 cm
- Puissance du laser : 60 W
- Type de LASER : Tube CO2 (infrarouge)

Le plus simple est d'utiliser un fichier dans un format .svg mais tout fichier pouvant être ouvert dans Inkscape peut être utilisé. Afin d'utiliser la machine, il faudra tout simplement ouvrir le fichier voulu dans Inkscape sur l'ordinateur lié à la machine et d'appuyer sur imprimer. Il faudra alors paramétrer les découpes (vitesse de passe et puissance du laser) en fonction du matériau utilisé et du résultat attendu (découpe ou gravure). Ces paramètres peuvent bien souvent être trouvé grâce à un reccueil de matériaux ayant été découpé à différentes puissances et vitesse.

La découpeuse Epilog a l'avantage de permettre un auto-focus en sélectionnant "Plunger" dans la liste des auto-focus. Un second avantage encore plus intéressant est sa caméra permettant d'observer si le motif qui va être découpé se trouve bel et bien sur le matériau.

### 4.3.2. Lasersaur
- Surface de découpe : 122 x 61 cm
- Hauteur maximum : 12 cm
- Vitesse maximum : 6000 mm/min
- Puissance du LASER : 100 W
- Type de LASER : Tube CO2 (infrarouge)

Cette machine est une découpeuse de grande taille d'où son nom. Elle est entièrement opensource et toute les informations nécessaire peuvent être trouvée [ici](https://www.lasersaur.com/).

La lasersaure utilise une interface de contrôle appelée Driveboard App et supportant les fichiers .dxf .svg et .dba.

Le détail de son utilisation sera expliqué plus tard dans la partie "4.4. Travail de groupe" mais un manuel reprenant toutes ces étapes peut être trouvé [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md).

### 4.3.3. Full Spectrum Muse

- Surface de découpe : 50 x 30 cm
- Hauteur maximum : 6 cm
- Puissance du LASER : 40 W
- Type de LASER : Tube CO2 (infrarouge) + pointeur rouge

Cette découpeuse de bien plus petite taille est à utiliser avec son propre ordinateur car aucun n'y est dédié dans le Fablab. Pour cela, il est soit possible de s'y connecter à l'aide d'un cable ethernet ou bien à l'aide d'un wifi dédié (Wifi: LaserCutter Mot de passe : fablabULB2019).
Une fois connecté il est possible d'accéder à l'interface de contrôle (Retina Engrave) en entrant l'adresse suivante http://fsl.local dans un navigateur. Cet interface accepte les fichiers vectoriels .svg mais également les fichiers matriciels de types BMP, JPEG, PNG, TIFF. Finalement les fichiers PDF étant matriciels et vectoriels sont acceptés. Le manuel de fonctionnement de Retina engrave est disponible [ici](http://laser101.fslaser.com/materialtest).

## 4.4. Travail de groupe : calibration (écrit par Sajl Ghani et modifié)
Pour ce cours, il a été demandé de former un groupe autour d'une des découpeuses laser. Nous avons donc formé un groupe avec Alessandro ABBRACCIANTE, Mats BOURGEOIS et Sajl Ghani.
Pour obtenir une bonne découpe du matériau, il est important de bien choisir les valeurs données au laser, à savoir :

- la puissance du laser
- la vitesse de découpe du laser

Il est aussi important de régler la hauteur par rapport à la surface du matériau pour avoir un bon focus (le point de croisement) sur la surface externe. La profondeur et le kerf (diamètre du rayon du laser) dépendrnt également des valeurs des paramètres mentionné ci-dessus donc il est nécessaire de faire une table de calibration.

Cette table de calibration reprenant les 2 paramètres principaux (vitesse et puissance) permet de voir quels réglages permettent une découpe et lesquels permettent une gravure (en haut à droite). Plusieurs tables de calibration sont disponibles pour chaque machine pour différents matériaux mais lors de l'utilisation d'un nouveau matériau, refaire une calibration est une étape importante.

Cette calibration a été faite avec la Lasersaure dont la hauteur de LASER est de 15mm sur du carton :

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/sajl.ghani/-/raw/main/docs/images/calibration_image.jpg)

Le fichier de calibration est disponible directement dans le Raspberry Pi lié à la Lasersaur.

## 4.5. Travail individuel
Après le travail de  groupe, un travail individuel qui consiste en la découpe d'un kirigami était demandé. Les kirigamis sont une variation des origamis. Là où les origamis consistent simplement en la création d'objets 3D en pliant une feuille de papier, les kirigamis utilisent non seulement le pliage mais aussi la découpe. Afin de créer mon kirigami j'ai d'abord chercher un design sur internet et je suis tombé sur des kirigami assez impressionant représentant des batiments, mais dans ces batiments la partie qui m'a le plus frapée était les différentes volées d'escaliers.
![](https://content.instructables.com/ORIG/FOZ/CQLQ/HO7XNBTF/FOZCQLQHO7XNBTF.jpg?auto=webp&frame=1&height=1024&fit=bounds&md=87823c218bbbe8e2b4f8a1ae6dd6fc5d)

Sur ce [site](https://www.instructables.com/Kirigami-Simple-Escher-Staircase/), avec l'image ci-dessus se trouve un tutoriel de comment réaliser un kirigami d'escaliers à la main en imprimant le fichier pdf donné. Etant donné que je ne voulais pas le découper à la main mais bien à la découpeuse laser et qu'importer le fichier pdf pour le vectoriser me semblait un peu trop facile, j'ai décidé de me baser dessus afin de créer mon propre kirigami d'escalier sur Inkscape.

J'ai donc du apprendre à utiliser Inkscape et surtout son outil de traçage de ligne. Pour cela, je me suis aidé des deux tutoriels suivant :

- [Tutoriel sur les bases d'Inkscape ](https://www.youtube.com/watch?v=GSGaY0-4iik&t=946s)
- [Tutoriel sur comment tracer des chemins](https://www.youtube.com/watch?v=C46g9xq5_R8)

En utilisant l'outil "Tracer des courbes de Bezier et des segments de droites" (entouré en rouge), j'ai donc premièrement tracé toutes les droites "de découpe" nécessaires à mon Kirigami de manière assez grossière.

![](../images/Module4/tracagedeligne.jpg)

Ensuite, en sélectionnant l'outil "Editer les noeuds ou les poignées de contrôle d'un chemin", j'ai sélectionné les extrémités de chacun des segments et j'ai défini les coordonées de celles-ci. Cette étape m'a permis d'avoir des lignes de découpe parallèles et des marches de hauteur régulières.

![](../images/Module4/coordonees.jpg)

Maintenant que toutes les lignes de découpe ont été effectuées, le travail pourrait être considéré comme terminé. Cependant, il sera nécessaire de plier le kirigami une fois la découpe effectuée. Afin de savoir où plier, j'ai également dessiné les "lignes de pli". Cela permet également de pouvoir graver (c'est à dire découper sur une partie de l'épaisseur seulement) la feuille ou le carton utilisé pour le kirigami afin de permettre de plier le matériau plus facilement. Les lignes de plis ont été effectuées en suivant le même processus que pour les lignes de découpe. Je les ai par contre dessinées dans une couleur différente (rouge) ce qui permet d'attribuer des paramètres de découpe différents pour ces lignes (permettant ainsi la gravure).

![](../images/Module4/kirigami_escalier.jpg)

Il ne reste ensuite plus qu'à découper le kirigami et à le plier et voici le résultat :

![](../images/Module4/kirigami.jpg)

Pour la découpe, la calibration faite au point 4.4 est utilisée pour savoir quels réglages permettent la découpe des lignes noires (par exemple 2300mm/min et 40W ou plus) et celle des lignes rouges (par exemple 2300 mm/min à 20 Watts ou moins). En général, la vitesse maximale est utilisée afin que la découpe soit plus rapide.
