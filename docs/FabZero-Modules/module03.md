# 3. Impression 3D

La séance de cette semaine a commencé par une présentation d'un professeur de la Faculté de Pharmacie. Celui-ci nous a introduit les différents types d'impression 3D ainsi que certains domaines dans lesquels cette technique de conception est utilisée. Le professeur en question travaille sur un processus d'impression de médicament afin que ceux-ci soient plus adaptés aux besoins du patient.

Durant ce cours les différents types de licenses ont également été abordé.

## 3.1. Types d'imprimantes

Il existe différent procédés d'impression 3D parmi lesquels on retrouve :

- la stéréolithographie
- le digital light processing
- le lit de poudre
- le goutte-sur-goutte
- le dépot de fondu qui est la méthode utilisée par les imprimantes du FabLab et de la plupart des imprimantes 3D "classiques"

## 3.2. Utilisation des imprimantes Prusa

Afin d'utiliser les imprimantes Prusa, il faut utiliser le logiciel PrusaSlicer qui permet de régler tous les paramètres nécessaires à une bonne impression à savoir la hauteur des couches, le remplissage, le motif d'impression, la création ou non d'une structure de support, ... Le logiciel, comme son nom l'indique, découpe ensuite l'objet à imprimer en tranches afin que l'imprimante puisse faire son travail. En effet, les imprimantes à dépot de fondu, déposent du plastique fondu couche par couche afin de créer l'objet attendu.

Les imprimantes du FabLab sont des imprimantes Prusa I3 MK3 et MK3S ayant un plateau de 22,5x22,5cm et offrant une hauteur d'impression de 25cm.

Afin d'imprimer un modèle à l'aide d'une imprimante Prusa, il faut donc d'abord importer celui-ci dans PrusaSlicer (en format .stl) et faire tous les réglages nécessaires. Les plus importants sont le remplissage et le motif d'impression qui affecteront la résistance de la pièce. Il est également important de choisir correctement le filament utilisé, les caractéristiques des couches et s'il est nécessaire ou nom de créer des supports (nécessaire pour ne pas imprimer dans le vide). Les valeurs classiques des différents paramètres peuvent être trouvés [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/class/-/blob/master/vade-mecum/3D_print.md). Une fois tous les réglages effectués, un G-code peut être généré et importé dans une carte SD afin de le fournir à l'imprimante. Une fois sur cette dernière, l'impression peut être lancée. Il est toutefois primordial de toujours vérifier qu'il reste assez de filament sur la bobine et si ce n'est pas le cas, de changer cette dernière. Il faut aussi vérifier qu'il n'y pas de noeuds dans la bobine et finalement il faut nettoyer le plateau avant chaque impression afin de permettre une bonne adhesion de l'imprimé à celui-ci.


## 3.3. Travail de groupe : torture test (écrit par Sajl Ghani puis modifié)

Le travail de groupe consiste en l'impression d'un torture test. Un torture test est un test permettant entre autres de vérifier la capacité de l'imprimante 3D à effectuer différentes tâches telles que l'impression "dans le vide" sous différents angles, sa précision sur 1cm, ... Un lien vers le .stl d'un torture test est disponible [ici](https://www.thingiverse.com/thing:2806295).
Les paramètres choisis sont les mêmes que dans le lien de la section précédente avec pour seules différences : le motif de remplissage fixé sur gyroide, le support qui a été enlevé et les dimensions ont été réduites à 80% afin de réduire le temps d'impression de 2h50 à 1h43. Le résultat obtenu est le suivant :
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/sajl.ghani/-/raw/main/docs/images/test_face.jpg)
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/sajl.ghani/-/raw/main/docs/images/test_droit.jpg)
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/sajl.ghani/-/raw/main/docs/images/test_gauche.jpg)

On peut voir que la plupart des tests ont été réussi par l'imprimante 3D même si l'impression sous différents angles n'est pas parfaite à 70 et 80°. Il est également important de se rappeler que le test a été fait à une échelle de 80% et que certains tests auraient pu se dérouler différemment si les longueurs étaient les longueurs initiales.


## 3.4. Travail personnel : kit de FlexLinks

Dans un deuxième temps, il est demandé d'imprimer un kit de FlexLinks. Pour cela, j'ai tout d'abord imprimé la pièce créée lors du Module 2 sur la conception assistée par ordinateur. Afin que les pièces puissent être fixées sur des Legos, les mesures suivantes ont été utilisées :
![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Lego_dimensions.svg/1280px-Lego_dimensions.svg.png)

Une distance de 8mm a été respectée entre le centre des trous et étant donné que les "plots" font 4,8mm de diamètre, j'ai décidé de faire des trous de 5mm de diamètre afin que les FlexLinks puissent être emboités sur les Legos sans être trop lâches mais tout en prenant une marge car dû à l'épaisseur du filament, il est nécessaire de prévoir un peu de marge.

Voici le résultat :


![](https://scontent.fbru5-1.fna.fbcdn.net/v/t1.15752-9/276089074_358957372818031_1265196674329654041_n.jpg?stp=dst-jpg_s1080x2048&_nc_cat=110&ccb=1-5&_nc_sid=ae9488&_nc_ohc=0956H7FoJ6sAX9wDmf4&_nc_ht=scontent.fbru5-1.fna&oh=03_AVLmBz-1bGQN5nBQoa_QIPrwP5g4NDSPjgUd1Pt93-53pQ&oe=625EEB64)

Ensuite, les 2 autres pièces que j'ai choisies sont celles créées par Théo Lisart en 2021 et trouvables [ici](https://gitlab.com/theo.lisart/parametric_flexlink). Il a créé un reccueil de plusieurs pièces ainsi que de différentes structures de base, utilisables pour modeler un grand nombre de pièces.

La première pièce choisie est le "double_z_spring" choisi car il offre une complexité supplémentaire. Pour celui-ci, j'ai du adapter les dimensions car les barres diagonales étaient trop fines et donc non reconnues par le logiciel PrusaSlicer. Le résultat obtenu est le suivant :

![](https://scontent.xx.fbcdn.net/v/t1.15752-9/276017454_662561691627095_2282365932927459705_n.png?stp=dst-png_p403x403&_nc_cat=106&ccb=1-5&_nc_sid=aee45a&_nc_ohc=WnCHBATJWi4AX9-HLrM&_nc_ad=z-m&_nc_cid=0&_nc_ht=scontent.xx&oh=03_AVJI91dknGCKnVzIR2E7wNgDKs2EfdN28M2oIyCsEZqkWw&oe=625D4939)

Finalement la troisième pièce de mon kit est assez similaire à la première mais avec un axe courbé afin de tester la capacité des imprimantes à imprimer des courbes, cette pièce est la "curved_flexlink".

Lors de l'impression de celle-ci, je n'avais pas fait gaffe que contrairement aux deux premières, la barre centrale ne touchait pas le plateau de l'imprimante. N'ayant pas utilisé de support, la première impression fut plutôt ratée. Une deuxième impression a alors été effectuée en ajoutant une structure. La première image montre ce support ajouté dans le logiciel PrusaSlicer et la deuxième montre l'impression de la pièce avec la structure de support.
![](../images/Module3/support.jpg)
![](https://scontent.fbru5-1.fna.fbcdn.net/v/t1.15752-9/276012472_469842504840203_8072144739153945667_n.jpg?stp=dst-jpg_s2048x2048&_nc_cat=104&ccb=1-5&_nc_sid=ae9488&_nc_ohc=fxs51zf8mV0AX-S2g-S&_nc_ht=scontent.fbru5-1.fna&oh=03_AVKRlQT-btmJ4IO1mJbtO0qH0hdyz72yGFrPupZOH3ZKfg&oe=625E34D0)
Sur la photo ci-dessous, on peut voir la différence entre la pièce créée avec un support et celle créée sans.
![](https://scontent.fbru5-1.fna.fbcdn.net/v/t1.15752-9/276023913_1142167509658256_1143869898866767076_n.jpg?stp=dst-jpg_s1080x2048&_nc_cat=100&ccb=1-5&_nc_sid=ae9488&_nc_ohc=hH96It5fjTQAX-nexBH&_nc_ht=scontent.fbru5-1.fna&oh=03_AVLJvmjcOPh-0VhIYKj7e7aVwiPP2aPVfvH9O4l3pXEuew&oe=625E7488)

## 3.5. Assemblage du kit

La dernière tache de ce module est celle de créer un assemblage à l'aide des FlexLinks imprimés et des Legos. Premièrement, les trous de 5mm de diamètre permettent aux pièces de s'emboiter parfaitement, des trous plus petits auraient été trop serrés pour permettre une adhesion des FlexLinks aux pièces Lego.

Pour ce kit, j'ai décidé de reproduire une pince du type pince à épiler.

![](https://www.meilleure-note.com/wp-content/uploads/2019/07/les-meilleures-pinces-a-epiler.jpg)
![](../images/Module3/pincekit.jpg)

Ici le "double_z_spring" permet de garder les 2 pièces proches l'une de l'autre tout en permettant un mouvement. Le "curved_flexlink" permet de son côté d'avoir une force de rappel assez grande vers la position de stabilité "pince ouverte".


<video width="500" controls src="../../videos/videopincekit.mp4" type="video/mp4">
Résultat
</video>

## 3.6. Les différents types de licenses

Lorsqu'un travail est publié, il est important de notifier sous quelles conditions ce travail peut être réutilisé ou non. Pour cela, il existe différentes licences que l'on peut retrouver [ici](https://creativecommons.org/about/cclicenses/) :

- CC BY : en utilisant cette license, le travail peut être réutilisé et modifié, tant que les crédits sont donnés.
- CC BY-SA : de la même manière, cette license permet de réutiliser et de modifier le travail mais le travail modifié doit être publié sous les mêmes termes que le travail de base.
- CC BY-NC : cette license est identique à la licence CC BY, à la seule différence que le travail ne peut pas être utilisé à des fins commerciales.
- CC BY-NC-SA : cette license est un mix des deux précédentes.
- CC BY-ND : cette license permet d'utiliser le travail mais pas de l'adapter, les crédits doivent être donnés à l'auteur.
- CC BY-NC-ND : un travail sous cette license ne peut ni être adapté, ni être utilisé à des fins commerciales.
- CC0 : le travail est publié dans le domaine public, le travail peut donc être utilisé sans aucune condition.

Le travail de Théo Lisart récupéré à la section précédente est sous MIT License, ce qui correspond à une license CC BY, c'est pourquoi il m'était nécessaire de mentionner le nom de Théo Lisart lors de son utilisation.
