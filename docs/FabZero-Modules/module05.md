# 5. Electronique 1 - Prototypage

Cette semaine, le prototypage à l'aide d'un microcontrolleur Arduino a été abordé. Les microcontrolleurs Arduino sont comme mentionné précédemment destiné à du prototypage ce qui signifie qu'ils ne sont pas faits pour être intégrés à des systèmes. Nous avons ici travaillé avec un Arduino UNO. Tous les codes arduinos ci-dessous peuvent être trouvé [ici](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/alex.stoesser/-/tree/main/docs/Files/Arduinos).

## 5.1. Précautions
Il est important de prendre certaines précautions lors de l'utilisation d'un tel microcontrolleur.

Premièrement, la carte peut supporter un certain courant maximum par pin (40mA), par groupes de pin (100mA) et un maximum pour l'entièreté de la carte (200mA). Afin de s'assurer de quels pins appartiennent ou non au même groupe, [ce schéma](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero/electronics/-/raw/master/img/Arduino-uno-pinout.png) peut être utilisé.

Deuxièmement, il est important de toujours débrancher l'Arduino de son ordinateur lorsque l'on effectue des branchements car un court-circuit pourrait bruler le port USB de notre PC et le rendre inutilisable.

## 5.2. Schéma PINOUT

Le schéma utilisé à la section précédente montre quelles sont les pins digitales et analogues. Une pin digitale permet l'acquisition de données sous forme de 1 ou de 0, c'est à dire qu'elle observe soit un voltage de 5V soit de 0V. Contrairement aux pins digitales, les pins analogues permettent la réception ou l'envoi de valeur comprises respectivement entre 0 et 1023 et entre 0 et 255.

![](https://www.researchgate.net/profile/Yekini-Nureni/publication/288180515/figure/fig21/AS:607816750473224@1521926214764/Analog-vs-Digital-Signal.png)

Toutefois il est possible de créer un signal sinusoidal depuis une pin digitale à l'aide de la Pulse Width Modulation (PWM) qui consiste en l'envoi de 1 et de 0 sur une période plus ou moins longue de manière à ce que la moyenne de ces signaux forme une sinusoide. Les pins digitales permettant la PWM sont indiquées avec un \#.

![](https://www.researchgate.net/profile/Manel-Hammami-2/publication/319071311/figure/fig4/AS:631655408611389@1527609793990/Ideal-pulse-width-modulation-PWM-inverter-output-voltage-instantaneous-component-blue.png)


## 5.3. IDE et code

Le langage de programmation utilisé sur un module Arduino est le C++. Un script arduino est toujours composé de la même manière, à savoir premièrement, l'initialistion des variables, puis le "setup" qui va permettre d'exécuter les fonctions qui doivent l'être qu'une seule fois au début du code comme par exemple si une pin est utilisée comme Input ou Output. Finalement la "loop" dans laquelle sera écrit le code qui doit s'exécuter en continu. Ci-dessous se trouve un exemple de structure :

```

int sensor = numero du port;

void setup(){
  ce qu'on doit juste faire une fois au début, par exemple démarrer un capteur
}

void loop(){
  ce qui va être répété en boucle, ce que l'on demande à l'arduino de faire
}

```

Les différentes fonctions et commandes qui doivent être fournies à un module Arduino sont très faciles à prendre en main car assez implicites.

Par exemple, pour récupérer une information, selon qu'elle provienne d'une pin analogue ou digitale, il suffit respectivement d'utiliser les fonctions :

```
analogRead()
```
et
```
digitalRead()
```

De la même manière, pour envoyer une information à ces mêmes pins, on utilise :

```
analogWrite()
```
et
```
digitalWrite()
```

Pour initaliser une pin, on utilisera la fonction
```
pinMode()
```
dans laquelle on mentionnera premièrement quelle pin on utilise puis à l'aide d'une des variables INPUT ou OUTPUT à quelle fin la pin devra être utilisée, à savoir respectivement pour récupérer ou pour envoyer des données.

Il existe également des variables telles que HIGH et LOW qui permettent l'envoi d'un haut ou faible voltage par exemple pour faire briller une LED qui sont elles à utiliser dans un digitalWrite() par exemple.

Finalement, la fonction
```
delay()
```
dans la quelle on mentionne un certain temps, permet d'attendre dans l'état actuel des choses. Ce qui permet par exemple après avoir allumé une LED d'attendre un certain temps avec celle-ci allumée avant de donner l'ordre de l'éteindre.
Il est important de savoir que l'échelle de temps de l'arduino est la milliseconde.


De nombreuses fonctions utiles peuvent être trouvées sur [ce site](https://www.arduino.cc/reference/fr/).
Sur l'Arduino IDE qui peut être téléchargé [ici](https://www.arduino.cc/en/software), il est également possible de trouver des exemples de code pour de nombreuses applications en cliquant dans fichiers puis exemple :

![](../images/Module5/exemple.jpg)

Il est par exemple possible en cliquant sur 01.Basics puis Blink de trouver un code permettant de faire clignoter une LED qui est le suivant :
```
/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/Blink
*/

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```

Il est ainsi possible d'observer comment initialiser une LED, en utilisant la fonction pinMode(). Ici, aucune "pin", n'est utilisée car la LED utilisée est LED_BUILTIN qui est une LED déjà soudée sur l'Arduino et puis il est essentiel de notifier que cette LED sera utilisée comme OUTPUT.

Dans la loop, on utilise ensuite la fonction digitalWrite() associée à la LED en question, LED_BUILTIN, que l'on va allumer grâce à l'état HIGH et l'éteindre grâce à l'état LOW. Entre ces deux états, l'Arduino attend 1 seconde soit 1000 millisecondes grâce à la fonction delay().


## 5.4. Différents types d'INPUT et d'OUTPUT

Après avoir pris en main la manière de fonctionner d'un Arduino, il est assez facile d'adapter le code pour utiliser différents INPUT et OUTPUT. Les INPUT seront en général des capteurs tandis que les OUTPUT sont des éléments permettant une action que ce soit une LED qui s'allume, un ServoMoteur, un écran, un électro-aimant ou bien d'autres.

Les différents types d'entrées et de sorties disponibles au Fablab sont listées sur [cette page gitlab](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero/electronics/-/blob/07e9c48531529f84506c966be76957e4dba6ec1c/IO-Modules.md) et accompagnées de leur datasheet. Il est très important de lire cette datasheet non seulement pour savoir comment alimenter et récupérer/envoyer des données de/à ces composants et donc comment effectuer leur branchement. Mais aussi, pour l'alimenter à la bonne tension afin d'éviter de brûler le composant. Finalement, étant donné que les capteurs analogues renvoient des données à l'aide de nombres compris entre 0 et 1023, il est impératif de savoir trouver l'équivalence entre ces chiffres et la mesure réelles afin de pouvoir exploiter les données. Idem pour une sortie analogue qui reçoit des valeurs entre 0 et 255.

Par exemple un capteur de distance qui renvoie une valeur de 452 peut, selon ses réglages indiquer une distance de 40 centimètres comme de 3,86 mètres. Ou de la même manière, un servomoteur, qui fonctionnerait à 360 degrés, devra recevoir une valeur d'angle entre 0 et 255. Il est alors plus que probable qu'un degré corresponde à une valeur de 255/360 soit 0,708.


## 5.5. Mes réalisations
### 5.5.1. Allumer une led à l'aide d'une photorésistance

Après avoir tester le code de la section 5.3. qui permet de faire clignoter une LED, j'ai voulu expérimenter l'utilisation d'un capteur, à savoir une photorésistance. Celle-ci doit être branchée sur une pin analogue et renvoie une valeur en fonction de lumière captée.

Le code que j'ai créé était alors celui-ci :

```

/*
File : "sketch_mar17b_blink.ino"

Author : Alexandre Stoesser <alexandre.stoesser@ulb.be>

License : CC BY (https://creativecommons.org/licenses/by/4.0/)

*/

int led = 3;
int sensor= A1;

void setup() {
  // put your setup code here, to run once:

Serial.begin(9600);
pinMode(led, OUTPUT);
pinMode(sensor, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  // read the input pin:
  int sensorState = analogRead(sensor);
  // print out the state of the button:
  Serial.println(sensorState);
  delay(1);        // delay in between reads for stability

  if (sensorState > 800){
  digitalWrite(led, LOW);   // turn the LED on (HIGH is the voltage level)                 
  }

  else {
    digitalWrite(led, HIGH);    // turn the LED off by making the voltage LOW

    }             
}

```




Dans ce code contrairement à celui de l'exemple d'Arduino, je commence tout d'abord par nommer mes pins afin de travailler plus facilement par la suite. Je note donc tout d'abord que ma "led" est branchée sur la pin 3 (pin digitale) et ensuite  que mon "sensor" est branché sur la pin A1 (pin analogue). Ensuite, dans le setup, je démarre le Serial qui permet de lire les valeurs renvoyées par le capteur et j'initialise les pins de la led et du sensor respectivement comme une sortie et une entrée. Dans la loop, je commence par lire la valeur renvoyée par le capteur grace à analogRead(). J'imprime ensuite cette valeur dans le serial à l'aide de Serial.println(). Finalement, la LED est éteinte si la valeur renvoyée par le capteur est au dessus de 800, dans le cas contraire la LED est allumée. J'ai choisi la valeur de 800 car la valeur renvoyée dû à la lumière ambiante se trouve au dessus de celle-ci. La LED s'allumera donc si la photorésistance est couverte.


![](../images/Module5/montage1.1.png)

On observe sur cette photo du cablage que la led est bien branchée au port numéro 3. Pour brancher une LED il est également important de mettre en série avec celle-ci une résistance afin d'éviter de la griller, le circuit doit bien évidemment finir à la terre ou ground (gnd). Il est important de se rappeler qu'une diode et donc une LED (Light Emitting Diode) ne laisse passer le courant que dans un sens, la plus grande patte de la diode doit être du côté du positif. Du point de vue du capteur, il y a normalement sur tous les sensors 3 pins, une à brancher au positif (5 volts), une à brancher à la terre et une permettant de faire des mesures générallement celle du milieu. La photorésistance n'ayant que 2 port, il va falloir créer ces 3 ports à l'aide d'un diviseur résistif. Ici après vérification grâce à un multimètre, la photorésistance à une résistance de 100 Ohms, une résistance de 100 Ohms est donc mise en série avec celle-ci. Nous avons donc un côté branché au positif et un à la terre, entre les 2 un cable permet la lecture et est branché ici au port A1.

Voici finalement le résultat :



<video width="500" controls src="../../videos/outputtest1.mp4" type="video/mp4">
Résultat
</video>

Par la suite, j'ai voulu tester la capacité de la photorésistance à capter le flash d'un téléphone. Pour cela, j'ai refait le système en utilisant 3 LEDS correspondant à 3 états : lumière ambiante, lumière faible, lumière forte.

```
/*
File : "sketch_mar17b_blink3levels.ino"

Author : Alexandre Stoesser <alexandre.stoesser@ulb.be>

License : CC BY (https://creativecommons.org/licenses/by/4.0/)

*/

int lednormal = 3;
int ledhigh = 4;
int ledlow = 5;
int sensor= A1;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
pinMode(ledhigh, OUTPUT);
pinMode(ledlow, OUTPUT);
pinMode(lednormal, OUTPUT);
pinMode(sensor, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  // read the input pin:
  int buttonState = analogRead(sensor);
  // print out the state of the button:
  Serial.println(buttonState);
  delay(1);        // delay in between reads for stability




  if (buttonState > 900){
  digitalWrite(ledlow, HIGH);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(lednormal, LOW);    // turn the LED off by making the voltage LOW  
  digitalWrite(ledhigh, LOW);    // turn the LED off by making the voltage LOW                     

  }
  else if(buttonState<900 && buttonState > 800)  {
  digitalWrite(lednormal, HIGH);    // turn the LED off by making the voltage LOW
  digitalWrite(ledlow, LOW);    // turn the LED off by making the voltage LOW
  digitalWrite(ledhigh, LOW);    // turn the LED off by making the voltage LOW                     

  }         
  else {
    digitalWrite(lednormal, LOW);    // turn the LED off by making the voltage LOW
  digitalWrite(ledlow, LOW);    // turn the LED off by making the voltage LOW
  digitalWrite(ledhigh, HIGH);    // turn the LED off by making the voltage LOW     
    }             
}

```

Le fonctionnement général est exactement le même à la différence près qu'ici 3 LEDS sont utilisées et allumées sous des conditions différentes.

![](../images/Module5/montage3LED.jpg)

Le cablage est également similaire. Voici le résultat :



<video width="500" controls src="../../videos/output3LED.mp4" type="video/mp4">
Résultat
</video>

Finalement afin de tester son fonctionnement, j'ai remplacé la photorésistance par un potentiomètre. Celui-ci a 3 pins et doit être branché de la même manière que la photorésistance à la différence près qu'aucun diviseur résistif est nécessaire. Le code est totalement similaire.

![](../images/Module5/potentiometre.jpg)



<video width="500" controls src="../../videos/outputpotentiomètre.mp4" type="video/mp4">
Résultat
</video>

### 5.5.2. Eteindre un interrupteur lorsqu'il fait assez clair

Afin de tester le fonctionnement d'un ServoMoteur, j'ai mis au point un petit système capable d'appuyer sur un interrupteur afin d'éteindre la lumière si il fait assez clair.

```
/*
File : "sketch_mar17b_servowithlight.ino"

Author : Alexandre Stoesser <alexandre.stoesser@ulb.be>

License : CC BY (https://creativecommons.org/licenses/by/4.0/)

*/
#include <Servo.h>
Servo myservo;  // create servo object to control a servo
int sensor= A1;

void setup() {
  // put your setup code here, to run once:

Serial.begin(9600);
myservo.attach(A0);  // attaches the servo on pin 9 to the servo object
pinMode(sensor, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  // read the input pin:
  int buttonState = analogRead(sensor);
  // print out the state of the button:
  Serial.println(buttonState);
  delay(1);        // delay in between reads for stability

  int Off =250;
  int On = 10;
  if (buttonState > 00){
  myservo.write(Off);                  
  }
  else {
  myservo.write(On);                   
  }                      
}

```
Ici, je commence par initialiser le ServoMoteur. Ensuite dans le setup, je définis sur quelle pin est attaché ce moteur, ici bien évidemment une pin analogue. Finalement dans la loop, je définis la position à laquelle le ServoMoteur se place lorsqu'il doit éteindre la lumière et sur quelle position il se place sinon.

![](../images/Module5/servo.jpg)

Par soucis de simplicité, le niveau de lumière avec lampe allumée a été défini en dessous de 800 et donc le niveau de luminosité pour lequel la lumière ne doit plus être allumée a été fixé à 800 (le soleil est représenté par la lumière ambiante) mais dans le cas d'une utilisation réelle, il faudrait bien sûr le calibrer autrement et par exemple mettre la photorésistance plus proche d'une fenêtre afin qu'elle soit en effet influencée par la lumière du soleil.

Voici ainsi le résultat :


<video width="500" controls src="../../videos/videoservo.mp4" type="video/mp4">
Résultat
</video>



## 5.6. Effectuer une mesure

La dernière étape demandée est d'effectuer une mesure à l'aide d'un capteur. La photorésistance que j'ai utilisée depuis le début de ce Module me renvoie des valeurs (environ 800 à la lumière ambiante) comprise entre 0 et 1023. Cependant, ce n'est pas une mesure d'une valeur réelle. Une mesure serait dès lors donnée en lux. Pour savoir à combien de lux chaque valeur comprise entre 0 et 1023 correspond, il faut trouver l'échelle entre les deux (qui n'est pas forcément linéaire). Une telle échelle peut en général être trouvée dans la datasheet. Dans le cas contraire, il est toujours possible d'effectuer des mesures à l'aide d'un instrument de mesure sous différentes conditions et d'en extrapoler la courbe de l'échelle recherchée. N'ayant pas de référence de la photorésistance, il ne m'était pas possible de trouver sa datasheet. J'ai donc décidé de me tourner vers un autre capteur : un capteur de distance.

Le capteur de distance est un VL53L0X, un capteur dit "de temps de vol". Pour pouvoir l'utiliser et avoir un exemple de code arduino, il m'a fallu télécharger le firmware disponible [ici](https://www.velleman.eu/support/downloads/?code=VMA337) avec la datasheet. Pour savoir comment l'ajouter, regarder dans le [manuel d'utilisation](https://www.velleman.eu/downloads/29/vma337_a4v01.pdf) : dans l'Arduino IDE, aller dans Sketch puis Include Library et finalement Add .ZIP Library.

Le capteur est ensuite à brancher au Vin et à la masse et les pins SCL et SDA doivent être branchées à des pins analogues comme sur les photos suivantes :  

![](../images/Module5/branchement1.jpg)
![](../images/Module5/branchement2.jpg)

En plus du capteur, j'ai également branché une LED.

Contrairement à ce que j'ai expliqué ci-dessus, pour transformer les valeurs analogiques renvoyées par le capteur en valeur mesurable nul besoin de chercher une courbe correspondante dans la datasheet avec ce capteur. En effet, la bibliothèque téléchargée ci-dessus effectue directement cette transformation et l'on récupère dès lors une valeur très précise en millimètres dans l'affichage "Serial".

Pour pouvoir visuellement vérifier le fonctionnement du capteur, je l'ai lié à une LED qui s'allume si il capte un objet à une distance moindre que 10 cm.

Le code est principalement basé sur l'exemple "continous" fourni avec la bibliothèque.
```
/*
File : "capteurdedistance.ino"

Author : Alexandre Stoesser <alexandre.stoesser@ulb.be>

License : CC BY (https://creativecommons.org/licenses/by/4.0/)

*/
#include <Wire.h>
#include <VL53L0X.h>

VL53L0X sensor;
int led = 2;
int mm;

void setup(){
  Serial.begin(9600);
  Wire.begin();

  sensor.init();
  sensor.setTimeout(500);

  // Start continuous back-to-back mode (take readings as
  // fast as possible).  To use continuous timed mode
  // instead, provide a desired inter-measurement period in
  // ms (e.g. sensor.startContinuous(100)).

  sensor.startContinuous();
  pinMode(led, OUTPUT);
  }

void loop(){
  mm=sensor.readRangeContinuousMillimeters();
  Serial.print(mm);
  if (sensor.timeoutOccurred()) { Serial.print(" TIMEOUT"); }

  Serial.println();

    if (mm < 100){
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  }

  else {
    digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
    }  
}

```

Le résultat obtenu est dès lors :


<video width="500" controls src="../../videos/capteurdistancevidéo.mp4" type="video/mp4">
Résultat
</video>

Finalement voici le retour (en mm) sur le serial pour un objet placé exactement à 10cm du capteur. Etant donné que je tenais l'objet en main et qu'il m'était difficile de rester exactement à 10 centimètres sans trembler, on peut voir une légère oscillation autour des 100 mm.

![](../images/Module5/serial.png)
